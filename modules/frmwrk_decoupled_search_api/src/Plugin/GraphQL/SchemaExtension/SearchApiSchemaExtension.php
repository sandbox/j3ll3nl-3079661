<?php

namespace Drupal\frmwrk_decoupled_search_api\Plugin\GraphQL\SchemaExtension;

use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;

/**
 * Class SearchApiSchemaExtension.
 *
 * @SchemaExtension(
 *   id = "search_api_extension",
 *   name = "Search api extension",
 *   description = "Extend schema with a search implementing search api",
 *   schema = "frmwrk"
 * )
 *
 * @package Drupal\frmwrk_decoupled_search_api\Plugin\GraphQL\SchemaExtension
 */
class SearchApiSchemaExtension extends SdlSchemaExtensionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry): void {
    $builder = new ResolverBuilder();

    $this->addSearchApi($registry, $builder);
  }

  /**
   * Add search api and fields to the query.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   Resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   Resolver builder.
   */
  private function addSearchApi(ResolverRegistryInterface $registry, ResolverBuilder $builder): void {
    $registry->addFieldResolver('Query', 'search',
      $builder->produce('search_nodes')
        ->map('searchValue', $builder->fromArgument('searchValue'))
        ->map('searchFields', $builder->fromArgument('searchFields'))
        ->map('languages', $builder->fromArgument('languages'))
        ->map('offset', $builder->fromArgument('offset'))
        ->map('limit', $builder->fromArgument('limit'))
        ->map('conditions', $builder->fromArgument('conditions'))
        ->map('sortDirection', $builder->fromArgument('sortDirection'))
        ->map('sortField', $builder->fromArgument('sortField'))
    );
  }

}
