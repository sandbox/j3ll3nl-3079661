<?php

namespace Drupal\frmwrk_decoupled_search_api\Plugin\GraphQL\DataProducer;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\frmwrk_decoupled_search_api\Wrappers\SearchConnection;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\search_api\Entity\Index;
use Drupal\search_api\ParseMode\ParseModePluginManager;
use GraphQL\Error\UserError;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SearchNodes.
 *
 * @DataProducer(
 *   id = "search_nodes",
 *   name = @Translation("Search nodes"),
 *   description = @Translation("Loads a list of nodes."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Collection of nodes matching the search filter.")
 *   ),
 *   consumes = {
 *     "searchValue" = @ContextDefinition("string",
 *       label = @Translation("Search text."),
 *       required = FALSE
 *     ),
 *     "searchFields" = @ContextDefinition("list",
 *       label = @Translation("Search fields."),
 *       required = FALSE
 *     ),
 *     "languages" = @ContextDefinition("list",
 *       label = @Translation("Language to search in."),
 *       required = FALSE
 *     ),
 *     "offset" = @ContextDefinition("integer",
 *       label = @Translation("Search offset."),
 *       required = FALSE
 *     ),
 *     "limit" = @ContextDefinition("integer",
 *       label = @Translation("Search limit."),
 *       required = FALSE
 *     ),
 *     "conditions" = @ContextDefinition("list",
 *       label = @Translation("Search conditions."),
 *       required = FALSE
 *     ),
 *     "sortField" = @ContextDefinition("string",
 *       label = @Translation("Sorting field."),
 *       required = FALSE
 *     ),
 *     "sortDirection" = @ContextDefinition("string",
 *       label = @Translation("Sorting direction (ASC/DESC)."),
 *       required = FALSE
 *     )
 *   }
 * )
 */
class SearchNodes extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  const MAX_LIMIT = 100;
  const SORT_ASC = "ASC";
  const SORT_DESC = "DESC";
  const QUERY_OPERATORS = [
    "EQUALS_TO" => "=",
    "NOT_EQUAL_TO" => "!=",
  ];

  /**
   * Parse mode plugin manager.
   *
   * @var \Drupal\search_api\ParseMode\ParseModePluginManager
   */
  protected $parseModePluginManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.search_api.parse_mode')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    ParseModePluginManager $parseModePluginManager
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);

    Index::load('');

    $this->parseModePluginManager = $parseModePluginManager;
  }

  /**
   * Node search api.
   *
   * @param string|null $searchValue
   *   Search term including.
   * @param array|null $searchFields
   *   Search fields to match on.
   * @param array|null $languages
   *   Languages to search in.
   * @param string|null $offset
   *   Offset.
   * @param string|null $limit
   *   Limit.
   * @param array|null $conditions
   *   Query conditions.
   * @param string|null $sortField
   *   Field to sort on.
   * @param string|null $sortDirection
   *   Sort direction (ASC/DESC)
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $metadata
   *   Cache metadata.
   *
   * @return \Drupal\frmwrk_decoupled_search_api\Wrappers\SearchConnection
   *   Search connection.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   If the instance cannot be created, such as if the ID is invalid.
   */
  public function resolve($searchValue, $searchFields, $languages, $offset, $limit, $conditions, $sortField, $sortDirection, RefinableCacheableDependencyInterface $metadata): SearchConnection {
    $index = Index::load('content_search');
    /** @var \Drupal\search_api\Query\QueryInterface $query */
    $query = $index->query();
    /** @var \Drupal\search_api\ParseMode\ParseModeInterface $parseMode */
    $parseMode = $this->parseModePluginManager->createInstance('direct');
    $parseMode->setConjunction('OR');
    $query->setParseMode($parseMode);

    $query->addCondition('status', 1);

    if (isset($offset) && isset($limit)) {
      if (!$limit > static::MAX_LIMIT) {
        throw new UserError(sprintf('Exceeded maximum query limit: %s.', static::MAX_LIMIT));
      }

      $query->range($offset, $limit);
    }
    else {
      $query->range(0, self::MAX_LIMIT);
    }

    if (isset($searchValue) && isset($searchFields)) {
      $query->setFulltextFields($searchFields);
      $query->keys($searchValue);
    }

    if (isset($conditions)) {
      foreach ($conditions as $condition) {
        // Convert GraphQL argument to a query operator.
        $operator = self::queryOperator($condition['operator']);
        $query->addCondition($condition['field'], $condition['value'], $operator);
      }
    }

    if (isset($languages)) {
      $query->setLanguages($languages);
    }

    if (isset($sortField)) {
      if (!isset($sortDirection) || !in_array($sortDirection, [self::SORT_ASC, self::SORT_DESC])) {
        $sortDirection = self::SORT_ASC;
      }

      $query->sort($sortField, $sortDirection);
    }

    $type = $index->getEntityType();
    $metadata->addCacheTags($type->getListCacheTags());
    $metadata->addCacheContexts($type->getListCacheContexts());

    return new SearchConnection($query);
  }

  /**
   * Checks if a query operator is permitted and translates it.
   *
   * @param string $search
   *   Graphql name for a query operator.
   *
   * @return string
   *   Query operator.
   */
  private static function queryOperator(string $search) {
    if (array_key_exists($search, self::QUERY_OPERATORS)) {
      return self::QUERY_OPERATORS[$search];
    }

    return self::QUERY_OPERATORS['EQUALS_TO'];
  }

}
