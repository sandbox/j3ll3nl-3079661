<?php

namespace Drupal\frmwrk_decoupled_search_api\Wrappers;

use Drupal\frmwrk_decoupled\Wrappers\ConnectionInterface;
use Drupal\search_api\Query\QueryInterface;
use GraphQL\Deferred;

/**
 * Class QueryConnection.
 */
class SearchConnection implements ConnectionInterface {

  /**
   * Query.
   *
   * @var \Drupal\Core\Entity\Query\Sql\Query
   */
  protected $query;

  /**
   * QueryConnection constructor.
   *
   * @param \Drupal\search_api\Query\QueryInterface $query
   *   Query.
   */
  public function __construct(QueryInterface $query) {
    $this->query = $query;
  }

  /**
   * {@inheritDoc}
   *
   * @throws \Drupal\search_api\SearchApiException
   *   Thrown if an error occurred during the search.
   */
  public function total(): int {
    $query = clone $this->query;
    return $query->range(NULL, NULL)->execute()->getResultCount();
  }

  /**
   * {@inheritDoc}
   *
   * @throws \Drupal\search_api\SearchApiException
   *   Thrown if an error occurred during the search.
   */
  public function items() {
    $result = $this->query->execute();
    if (empty($result)) {
      return [];
    }
    $result = $result->getResultItems();

    $buffer = \Drupal::service('graphql.buffer.entity');

    $callback = $buffer->add('node', array_values(array_map(function ($result) {
      return $result->getOriginalObject()->getEntity()->id();
    }, $result)));

    return new Deferred(function () use ($callback) {
      return $callback();
    });
  }

}
