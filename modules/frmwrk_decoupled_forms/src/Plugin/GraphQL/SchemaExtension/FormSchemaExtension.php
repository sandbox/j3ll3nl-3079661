<?php

namespace Drupal\frmwrk_decoupled_forms\Plugin\GraphQL\SchemaExtension;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;

/**
 * Class FormSchemaExtension.
 *
 * @SchemaExtension(
 *   id = "form_extension",
 *   name = "Paragraph extension",
 *   description = "Extend schema with node pragraphs and corresponding fields",
 *   schema = "frmwrk"
 * )
 *
 * @package Drupal\frmwrk_decoupled_forms\Plugin\GraphQL\SchemaExtension
 */
class FormSchemaExtension extends SdlSchemaExtensionPluginBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry): void {
    $builder = new ResolverBuilder();

    $this->addForm($registry, $builder);
  }

  /**
   * Adds forms to the schema.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   Resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   Resolver builder.
   */
  private function addForm(ResolverRegistryInterface $registry, ResolverBuilder $builder): void {
    $registry->addFieldResolver('Query', 'webform',
      $builder->produce('webform_json_form_resolver')
        ->map('webform', $builder->produce('webform_resolver')
          ->map('webformId', $builder->fromArgument('webformId')
        )
      )
    );
  }

}
