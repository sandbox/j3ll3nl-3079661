<?php

namespace Drupal\frmwrk_decoupled_forms\Plugin\GraphQL\DataProducer;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Access\CsrfRequestHeaderAccessCheck;
use Drupal\Core\Access\CsrfTokenGenerator;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\webform\Entity\Webform;
use Drupal\webform_jsonschema\Transformer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class WebformJsonFormResolver.
 *
 * @DataProducer(
 *   id = "webform_json_form_resolver",
 *   name = @Translation("webform"),
 *   description = @Translation("Return a webform's fields"),
 *   produces = @ContextDefinition("list",
 *     label = @Translation("webform object")
 *   ),
 *   consumes = {
 *     "webform" = @ContextDefinition("string",
 *       label = @Translation("Form id"),
 *       required = TRUE
 *     ),
 *   }
 * )
 *
 * @package Drupal\frmwrk_decoupled_media\Plugin\GraphQL\DataProducer
 */
class WebformJsonFormResolver extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * Webform to json schema transformer.
   *
   * @var \Drupal\webform_jsonschema\Transformer
   */
  private $jsonSchemaTransformer;

  /**
   * CSRF Token generator service.
   *
   * @var \Drupal\Core\Access\CsrfTokenGenerator
   */
  private $csrfTokenGenerator;

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition
  ) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('webform_jsonschema.transformer'),
      $container->get('csrf_token')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    Transformer $jsonSchemaTransformer,
    CsrfTokenGenerator $csrfTokenGenerator
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);

    $this->jsonSchemaTransformer = $jsonSchemaTransformer;
    $this->csrfTokenGenerator = $csrfTokenGenerator;
  }

  /**
   * Fetch the form fields.
   *
   * @param \Drupal\webform\Entity\Webform $webform
   *   Webform.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $metadata
   *   Cache metadata.
   *
   * @return array|\Drupal\Core\Entity\EntityInterface|null
   *   Web form submission.
   */
  public function resolve(Webform $webform, RefinableCacheableDependencyInterface $metadata) {
    $metadata->addCacheableDependency($webform);

    return [
      'schema' => Json::Encode($this->jsonSchemaTransformer->toJsonSchema($webform)),
      'ui' => Json::Encode($this->jsonSchemaTransformer->toUiSchema($webform)),
      'formData' => Json::encode([]),
      'buttons' => Json::Encode($this->jsonSchemaTransformer->toButtons($webform)),
      'csrfToken' => $this->csrfTokenGenerator->get(CsrfRequestHeaderAccessCheck::TOKEN_KEY),
      'submitUrl' => Url::fromUserInput(sprintf('/webform_jsonschema/%s', $webform->id()), [
        'absolute' => TRUE,
      ])->toString(),
      'formOptions' => [
        'status' => $webform->get('status') ?? NULL,
      ],
    ];
  }

}
