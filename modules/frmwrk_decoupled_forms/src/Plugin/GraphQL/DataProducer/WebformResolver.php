<?php

namespace Drupal\frmwrk_decoupled_forms\Plugin\GraphQL\DataProducer;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\webform\Entity\Webform;

/**
 * Class MediaFileResolver.
 *
 * @DataProducer(
 *   id = "webform_resolver",
 *   name = @Translation("webform"),
 *   description = @Translation("Return a webform's fields"),
 *   produces = @ContextDefinition("list",
 *     label = @Translation("webform object")
 *   ),
 *   consumes = {
 *     "webformId" = @ContextDefinition("string",
 *       label = @Translation("Form id"),
 *       required = FALSE
 *     ),
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("Webform parent entity"),
 *       required = FALSE
 *     ),
 *   }
 * )
 *
 * @package Drupal\frmwrk_decoupled_media\Plugin\GraphQL\DataProducer
 */
class WebformResolver extends DataProducerPluginBase {

  /**
   * Fetch the form.
   *
   * @param string|null $webformId
   *   Webform id.
   * @param \Drupal\Core\Entity\ContentEntityBase|null $entity
   *   Parent entity.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $metadata
   *   Cache metadata.
   *
   * @return \Drupal\webform\Entity\Webform|\Drupal\webform\Entity\Webform[]
   *   Webform instance.
   */
  public function resolve($webformId, $entity, RefinableCacheableDependencyInterface $metadata) {
    /** @var \Drupal\Core\Entity\ContentEntityBase $entity */
    if ($entity && $entity->hasField('field_webform')) {
      $cardinality = $entity->get('field_webform')->getFieldDefinition()->get('fieldStorage')->getCardinality();

      $webformIds = $entity->get('field_webform')->getValue();
      // Prevent returning a collection of arrays on one entity.
      if ($cardinality == 1) {
        /** @var \Drupal\webform\Entity\Webform $webform */
        $webform = Webform::load($webformIds[0]['target_id']);
        // Modify the status here as a webform attached to a entity can have a
        // status overriding the default status.
        $webform->set('status', $webformIds[0]['status']);
        $metadata->addCacheableDependency($webform);
        return $webform;
      }
      else {
        $output = [];
        foreach ($webformIds as $id) {
          /** @var \Drupal\webform\Entity\Webform $webform */
          $webform = Webform::load($id['target_id']);
          // Modify the status here as a webform attached to a entity can have a
          // status overriding the default status.
          $webform->set('status', $id['status']);
          $metadata->addCacheableDependency($webform);
          $output[] = $webform;
        }

        return $output;
      }
    }

    // Fallback to select a form if no parent entity is found.
    /** @var \Drupal\webform\Entity\Webform $webform */
    $webform = Webform::load($webformId);
    $metadata->addCacheableDependency($webform);
    return $webform;
  }

}
