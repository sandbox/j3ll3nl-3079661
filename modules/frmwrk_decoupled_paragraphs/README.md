## Notes for extending the paragraph modules

* Create a new module.

* Write a plugin based on Drupal\frmwrk_decoupled_paragraphs\FrmwrkDecoupledParagraphsPluginBase.
    > see `Drupal\frmwrk_decoupled_paragraphs\Plugin\FrmwrkDecoupledParagraphs\ImageParagraph` for the image paragraph 
    implementation.

* Write a graphQL description of your paragraph, location specified in the base plugin can be adjusted.

* Install the module.

* Cache rebuild.
