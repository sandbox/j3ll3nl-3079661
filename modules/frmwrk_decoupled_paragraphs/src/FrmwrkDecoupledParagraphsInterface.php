<?php

namespace Drupal\frmwrk_decoupled_paragraphs;

/**
 * Interface for frmwrk_decoupled_paragraphs plugins.
 */
interface FrmwrkDecoupledParagraphsInterface {

  /**
   * Returns the fields to be added to the paragraph section of a node.
   *
   * @return array
   *   Paragraphs with their fields.
   */
  public function addParagraphs(): array;

  /**
   * Return the graphql resolver for a paragraph bundle.
   *
   * @return array
   *   Paragraph types with respective resolver.
   */
  public function getParagraphTypeResolvers(): array;

  /**
   * Return the file location for the base .graphqls file.
   *
   * The path must not contain the file extension.
   *
   * @return string
   *   GraphQL base location.
   */
  public function getGraphqlBaseFile(): string;

  /**
   * Return the file location for the extension .graphqls file.
   *
   * The path must not contain the file extension.
   *
   * @return string
   *   GraphQL extension location.
   */
  public function getGraphqlExtensionFile(): string;

}
