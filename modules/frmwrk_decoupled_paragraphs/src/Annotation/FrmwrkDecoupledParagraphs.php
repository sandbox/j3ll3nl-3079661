<?php

namespace Drupal\frmwrk_decoupled_paragraphs\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines frmwrk_decoupled_paragraphs annotation object.
 *
 * @Annotation
 */
class FrmwrkDecoupledParagraphs extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
