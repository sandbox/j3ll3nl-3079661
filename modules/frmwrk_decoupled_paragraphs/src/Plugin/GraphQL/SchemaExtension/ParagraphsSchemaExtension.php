<?php

namespace Drupal\frmwrk_decoupled_paragraphs\Plugin\GraphQL\SchemaExtension;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\frmwrk_decoupled\services\NodeDetectorServiceInterface;
use Drupal\frmwrk_decoupled_paragraphs\FrmwrkDecoupledParagraphsPluginManager;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;
use Drupal\paragraphs\ParagraphInterface;
use GraphQL\Error\Error;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ParagraphsSchemaExtension.
 *
 * @SchemaExtension(
 *   id = "paragraph_extension",
 *   name = "Paragraph extension",
 *   description = "Extend schema with node pragraphs and corresponding fields",
 *   schema = "frmwrk"
 * )
 *
 * @package Drupal\frmwrk_decoupled_paragraphs\Plugin\GraphQL\SchemaExtension
 */
class ParagraphsSchemaExtension extends SdlSchemaExtensionPluginBase {

  use StringTranslationTrait;

  /**
   * Paragraph type with child values and resolver plugins.
   *
   * @var array
   */
  private $paragraphTypes = [
    'NotYetImplementedParagraph' => [
      'bundle' => 'paragraph_bundle_resolver',
    ],
  ];

  /**
   * Drupal paragraph types and their graphql resolver.
   *
   * @var array
   */
  private $paragraphTypeResolvers = [
    'NotYetImplementedParagraph' => 'NotYetImplementedParagraph',
  ];

  /**
   * GraphQL base files defined by paragraph extensions.
   *
   * @var array
   */
  private $gqlBaseFiles = [];

  /**
   * GraphQL extension files defined by paragraph extensions.
   *
   * @var array
   */
  private $gqlExtensionFiles = [];

  /**
   * Node detector service.
   *
   * @var \Drupal\frmwrk_decoupled\services\NodeDetectorServiceInterface
   */
  private $nodeDetectorService;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function __construct(
    $configuration,
    $pluginId,
    $pluginDefinition,
    ModuleHandlerInterface $moduleHandler,
    FrmwrkDecoupledParagraphsPluginManager $pluginManager,
    NodeDetectorServiceInterface $nodeDetectorService
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition, $moduleHandler);

    $this->nodeDetectorService = $nodeDetectorService;

    foreach ($pluginManager->getDefinitions() as $pluginDefinition) {
      /** @var \Drupal\frmwrk_decoupled_paragraphs\FrmwrkDecoupledParagraphsInterface $plugin */
      $plugin = $pluginManager->createInstance($pluginDefinition['id']);

      $key = key($plugin->addParagraphs());
      $this->paragraphTypes[$key] = array_merge($this->paragraphTypes[$key] ?? [], array_values($plugin->addParagraphs())[0]);
      $this->paragraphTypeResolvers = array_merge($this->paragraphTypeResolvers, $plugin->getParagraphTypeResolvers());

      $this->gqlBaseFiles[] = $plugin->getGraphqlBaseFile();
      $this->gqlExtensionFiles[] = $plugin->getGraphqlExtensionFile();
    }

    // Optionally dispatch an event here to make it possible to update some
    // of the values of $this->paragraphTypes && $this->paragraphTypeResolvers.
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition):ParagraphsSchemaExtension {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('plugin.manager.frmwrk_decoupled_paragraphs'),
      $container->get('frmwrk_decoupled.services.node_detector_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry): void {
    $builder = new ResolverBuilder();

    foreach ($this->nodeDetectorService->getNodeTypes() as $type) {
      $this->addParagraphTypeResolver($registry);
      $this->addParagraphs($type, $registry, $builder);
    }

    $registry->addFieldResolver('ContentParagraph', 'field_link',
      $builder->produce('link_object')
        ->map('link', $builder->fromParent())
        ->map('field', $builder->fromValue('field_link'))
    );

    $registry->addFieldResolver('VideoParagraph', 'field_link',
      $builder->produce('link_object')
        ->map('link', $builder->fromParent())
        ->map('field', $builder->fromValue('field_link'))
    );

    $registry->addFieldResolver('Link', 'uri',
      $builder->produce('url_path')
        ->map('url', $builder->produce('formatted_link_resolver')
          ->map('link_array', $builder->fromParent())
        )
    );

    $registry->addFieldResolver('FormParagraph', 'webform',
      $builder->produce('webform_json_form_resolver')
        ->map('webform', $builder->produce('webform_resolver')
          ->map('webformId', $builder->fromArgument('webformId'))
          ->map('entity', $builder->fromParent())
        )
    );

    foreach ($this->paragraphTypes as $type => $fields) {
      // Paragraph default fields, every paragraph type must implement.
      $this->addParagraphFields($type, [
        'parent_field_name' => 'paragraph_field_resolver',
        'id' => 'entity_id',
      ], $registry, $builder);

      // Custom paragraph fields defined in self::PARAGRAPH_TYPES.
      $this->addParagraphFields($type, $fields, $registry, $builder);
    }

    $this->addParagraphContentListFields($registry, $builder);
  }

  /**
   * Add interfaces.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   Resolver registry.
   */
  private function addParagraphTypeResolver(ResolverRegistryInterface $registry): void {
    $registry->addTypeResolver('ParagraphTypeInterface', function ($entity) {
      if ($entity instanceof ParagraphInterface) {
        // ContentListParagraph is a unique paragraph as it returns a
        // nodeConnection instead of regular fields.
        if ($entity->bundle() == "content_list") {
          return "ContentListParagraph";
        }

        // Returns the graphql implementation for a paragraph type or the
        // default if none is found.
        return $this->paragraphTypeResolvers[$entity->bundle()] ?? 'NotYetImplementedParagraph';
      }

      throw new Error($this->t('Could not resolve paragraph type.'));
    });
  }

  /**
   * Add paragraphs.
   *
   * @param string $type
   *   Node type.
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   Resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   Resolver builder.
   */
  private function addParagraphs(string $type, ResolverRegistryInterface $registry, ResolverBuilder $builder): void {
    // Add paragraphs to nodes consuming the node.
    $registry->addFieldResolver($type, 'paragraphs',
      $builder->produce('paragraphs')
        ->map('entity', $builder->fromParent())
    );
  }

  /**
   * Add ContentListParagraph.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   Resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   Resolver builder.
   */
  private function addParagraphContentListFields(ResolverRegistryInterface $registry, ResolverBuilder $builder): void {
    $registry->addFieldResolver('ContentListParagraph', 'nodes',
      $builder->produce('query_nodes')
        ->map('offset', $builder->fromArgument('offset'))
        ->map('bundles', $builder->fromArgument('bundles'))
        ->map('limit', $builder->fromArgument('limit'))
        ->map('sort', $builder->fromArgument('sort'))
        ->map('column', $builder->fromArgument('column'))
        ->map('paragraph', $builder->fromParent())
    );
  }

  /**
   * Add fields to paragraphs.
   *
   * @param string $parent
   *   Parent field.
   * @param array $fields
   *   Fields to be added under the defined parent field.
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   Resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   Resolver builder.
   */
  public function addParagraphFields(string $parent, array $fields, ResolverRegistryInterface $registry, ResolverBuilder $builder): void {
    foreach ($fields as $field => $resolver) {
      if (is_array($resolver)) {
        // Call this function again to add the array's children.
        $this->addParagraphFields($field, $resolver, $registry, $builder);

        // Set the resolver to use the array resolver.
        $resolver = 'paragraph_array_field_resolver';
      }

      // Map the graphql field to the drupal fields.
      $registry->addFieldResolver($parent, $field,
        $builder->produce($resolver)
          ->map('entity', $builder->fromParent())
          ->map('field', $builder->fromValue($field))
      );
    }

    $registry->addFieldResolver($parent, 'id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseDefinition() {
    return $this->appendFiles(parent::getBaseDefinition(), $this->gqlBaseFiles);
  }

  /**
   * {@inheritdoc}
   */
  public function getExtensionDefinition() {
    return $this->appendFiles(parent::getExtensionDefinition(), $this->gqlExtensionFiles);
  }

  /**
   * Append specified files to the graphQL file.
   *
   * @param string $file
   *   Paragraph gql definition.
   * @param array $locations
   *   File locations defined by plugins.
   *
   * @return string
   *   Graphql.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  private function appendFiles(string $file, array $locations) {
    foreach ($locations as $gqlFile) {
      if (!file_exists($gqlFile)) {
        throw new InvalidPluginDefinitionException(sprintf("Missing schema definition file at %s.", $gqlFile));
      }

      $file .= "\n" . file_get_contents($gqlFile);
    }

    return $file;
  }

}
