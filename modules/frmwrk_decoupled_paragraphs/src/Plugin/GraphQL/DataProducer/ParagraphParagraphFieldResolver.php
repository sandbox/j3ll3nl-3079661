<?php

namespace Drupal\frmwrk_decoupled_paragraphs\Plugin\GraphQL\DataProducer;

use Drupal\frmwrk_decoupled\Plugin\GraphQL\DataProducer\EntityReflectionBaseResolver;

/**
 * Class ParagraphParagraphFieldResolver.
 *
 * @DataProducer(
 *   id = "paragraph_paragraph_field_resolver",
 *   name = @Translation("paragraph paragraph field resolver"),
 *   description = @Translation("Resolve a specific paragraph field on a paragraph "),
 *   produces = @ContextDefinition("list",
 *     label = @Translation("Paragraph array field value(s)")
 *   ),
 *   consumes = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("Paragraph"),
 *       required = TRUE
 *     ),
 *    "field" = @ContextDefinition("string",
 *       label = @Translation("Field"),
 *       required = TRUE
 *     ),
 *   }
 * )
 *
 * @package Drupal\frmwrk_decoupled_paragraphs\Plugin\GraphQL\DataProducer
 */
class ParagraphParagraphFieldResolver extends EntityReflectionBaseResolver {

  /**
   * {@inheritDoc}
   */
  protected function getEntity(): string {
    return 'paragraph';
  }

}
