<?php

namespace Drupal\frmwrk_decoupled_paragraphs\Plugin\GraphQL\DataProducer;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\paragraphs\ParagraphInterface;

/**
 * Class ParagraphFieldResolver.
 *
 * @DataProducer(
 *   id = "paragraph_field_resolver",
 *   name = @Translation("paragraph field resolver"),
 *   description = @Translation("Resolve a specific paragraph field "),
 *   produces = @ContextDefinition("string",
 *     label = @Translation("Paragraph field value")
 *   ),
 *   consumes = {
 *     "entity" = @ContextDefinition("any",
 *       label = @Translation("Paragraph|Object"),
 *       required = TRUE
 *     ),
 *    "field" = @ContextDefinition("string",
 *       label = @Translation("Field"),
 *       required = TRUE
 *     ),
 *   }
 * )
 *
 * @package Drupal\frmwrk_decoupled_paragraphs\Plugin\GraphQL\DataProducer
 */
class ParagraphFieldResolver extends DataProducerPluginBase {

  /**
   * Resolves a paragraph field.
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraph
   *   Paragraph or paragraph field.
   * @param string $field
   *   Paragraph field to resolve.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $metadata
   *   Cache metadata.
   *
   * @return string|null
   *   Field value or null.
   */
  public function resolve(ParagraphInterface $paragraph, string $field, RefinableCacheableDependencyInterface $metadata) {
    $metadata->addCacheableDependency($paragraph);

    return $paragraph->hasField($field) ? $paragraph->get($field)->value : NULL;
  }

}
