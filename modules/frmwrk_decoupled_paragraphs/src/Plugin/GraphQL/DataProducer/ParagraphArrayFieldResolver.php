<?php

namespace Drupal\frmwrk_decoupled_paragraphs\Plugin\GraphQL\DataProducer;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\paragraphs\ParagraphInterface;

/**
 * Class ParagraphArrayFieldResolver.
 *
 * @DataProducer(
 *   id = "paragraph_array_field_resolver",
 *   name = @Translation("paragraph array field resolver"),
 *   description = @Translation("Resolve a specific paragraph array type field "),
 *   produces = @ContextDefinition("list",
 *     label = @Translation("Paragraph array field value(s)")
 *   ),
 *   consumes = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("Paragraph"),
 *       required = TRUE
 *     ),
 *    "field" = @ContextDefinition("string",
 *       label = @Translation("Field"),
 *       required = TRUE
 *     ),
 *   }
 * )
 *
 * @package Drupal\frmwrk_decoupled_paragraphs\Plugin\GraphQL\DataProducer
 */
class ParagraphArrayFieldResolver extends DataProducerPluginBase {

  /**
   * Resolve paragraph array field returning the contained props.
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraph
   *   Paragraph.
   * @param string $field
   *   Field to use.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $metadata
   *   Cache metadata.
   *
   * @return array
   *   Paragraph field data.
   */
  public function resolve(ParagraphInterface $paragraph, string $field, RefinableCacheableDependencyInterface $metadata): array {
    $metadata->addCacheableDependency($paragraph);

    return $paragraph->hasField($field) ? $paragraph->get($field)->getValue() : [];
  }

}
