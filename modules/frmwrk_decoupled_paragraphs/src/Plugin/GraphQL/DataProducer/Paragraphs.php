<?php

namespace Drupal\frmwrk_decoupled_paragraphs\Plugin\GraphQL\DataProducer;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\frmwrk_decoupled\DetectableFieldInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Paragraphs.
 *
 * @DataProducer(
 *   id = "paragraphs",
 *   name = @Translation("paragraph entity reference"),
 *   description = @Translation("Load all paragraphs base on a node"),
 *   produces = @ContextDefinition("list",
 *     label = @Translation("Paragraph")
 *   ),
 *   consumes = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("Entity"),
 *       required = TRUE
 *     ),
 *   }
 * )
 *
 * @package Drupal\frmwrk_decoupled_paragraphs\Plugin\GraphQL\DataProducer
 */
class Paragraphs extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Paragraph field name detector service.
   *
   * @var \Drupal\frmwrk_decoupled\DetectableFieldInterface
   */
  private $paragraphService;

  /**
   * Paragraph storage.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $paragraphStorage;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the entity type doesn't exist.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the storage handler couldn't be loaded.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, DetectableFieldInterface $paragraphService, EntityTypeManagerInterface $entityManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->paragraphService = $paragraphService;
    $this->paragraphStorage = $entityManager->getStorage('paragraph');
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the entity type doesn't exist.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the storage handler couldn't be loaded.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('frmwrk_decoupled.paragraph_field_detector'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Paragraph field resolver.
   *
   * @param \Drupal\node\Entity\Node $node
   *   Node entity.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $metadata
   *   Cache metadata.
   *
   * @return array
   *   Paragraphs.
   */
  public function resolve(Node $node, RefinableCacheableDependencyInterface $metadata): array {
    $metadata->addCacheableDependency($node);

    $fields = $this->paragraphService->getFieldNamesByBundle($node->bundle());

    foreach ($fields as $field) {
      foreach ($node->$field->getValue() as $paragraph) {
        $paragraph = $this->paragraphStorage->load($paragraph['target_id']);

        $metadata->addCacheableDependency($paragraph);
        $paragraphs[] = $paragraph;
      }
    }

    return $paragraphs ?? [];
  }

}
