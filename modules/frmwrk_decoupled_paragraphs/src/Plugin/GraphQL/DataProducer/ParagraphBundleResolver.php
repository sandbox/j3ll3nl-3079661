<?php

namespace Drupal\frmwrk_decoupled_paragraphs\Plugin\GraphQL\DataProducer;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\paragraphs\ParagraphInterface;

/**
 * Class ParagraphBundleResolver.
 *
 * @DataProducer(
 *   id = "paragraph_bundle_resolver",
 *   name = @Translation("paragraph bundle resolver"),
 *   description = @Translation("Resolves a paragraphs bundle"),
 *   produces = @ContextDefinition("string",
 *     label = @Translation("bundle name")
 *   ),
 *   consumes = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("Paragraph"),
 *       required = TRUE
 *     ),
 *    "field" = @ContextDefinition("string",
 *       label = @Translation("Field"),
 *       required = FALSE
 *     ),
 *   }
 * )
 *
 * @package Drupal\frmwrk_decoupled_paragraphs\Plugin\GraphQL\DataProducer
 */
class ParagraphBundleResolver extends DataProducerPluginBase {

  /**
   * Resolve paragraph array field returning the contained props.
   *
   * This bundle is a default resolver for the NotYetImplementedParagraph
   * providing an easy way to debug which paragraph bundle can't be resolved
   * yet.
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraph
   *   Paragraph.
   * @param string $_
   *   Field to use, unused but required for the definition.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $metadata
   *   Cache metadata.
   *
   * @return string
   *   Paragraph field data.
   */
  public function resolve(ParagraphInterface $paragraph, string $_, RefinableCacheableDependencyInterface $metadata): string {
    $metadata->addCacheableDependency($paragraph);

    return $paragraph->bundle();
  }

}
