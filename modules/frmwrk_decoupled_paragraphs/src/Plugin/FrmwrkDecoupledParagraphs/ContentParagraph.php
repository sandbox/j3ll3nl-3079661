<?php

namespace Drupal\frmwrk_decoupled_paragraphs\Plugin\FrmwrkDecoupledParagraphs;

use Drupal\frmwrk_decoupled_paragraphs\FrmwrkDecoupledParagraphsPluginBase;

/**
 * Plugin implementation of the frmwrk_decoupled_paragraphs.
 *
 * @FrmwrkDecoupledParagraphs(
 *   id = "content_paragraph",
 *   description = @Translation("Adds content type paragraphs.")
 * )
 *
 * @package Drupal\frmwrk_decoupled_paragraphs\Plugin\FrmwrkDecoupledParagraphs
 */
class ContentParagraph extends FrmwrkDecoupledParagraphsPluginBase {

  /**
   * {@inheritdoc}
   */
  public function addParagraphs(): array {
    // All paragraphs get their id and parent field name added to them so no
    // need to add it again.
    return [
      // Paragraph type.
      'ContentParagraph' => [
        // Drupal field machine name => GraphQL resolver plugin id.
        'field_image' => 'media_file_resolver',
        'field_content' => 'paragraph_field_resolver',
        'field_style_options' => 'paragraph_paragraph_field_resolver',
        'field_title' => 'paragraph_field_resolver',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getParagraphTypeResolvers(): array {
    return [
      // Drupal paragraph type => graphql interface implementation.
      'content' => 'ContentParagraph',
      'content_content' => 'ContentParagraph',
    ];
  }

}
