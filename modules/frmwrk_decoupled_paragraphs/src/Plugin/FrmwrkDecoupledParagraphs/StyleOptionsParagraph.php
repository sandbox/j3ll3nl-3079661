<?php

namespace Drupal\frmwrk_decoupled_paragraphs\Plugin\FrmwrkDecoupledParagraphs;

use Drupal\frmwrk_decoupled_paragraphs\FrmwrkDecoupledParagraphsPluginBase;

/**
 * Plugin implementation of the frmwrk_decoupled_paragraphs.
 *
 * @FrmwrkDecoupledParagraphs(
 *   id = "style_options_paragraph",
 *   description = @Translation("Adds style options type paragraphs.")
 * )
 *
 * @package Drupal\frmwrk_decoupled_paragraphs\Plugin\FrmwrkDecoupledParagraphs
 */
class StyleOptionsParagraph extends FrmwrkDecoupledParagraphsPluginBase {

  /**
   * {@inheritdoc}
   */
  public function addParagraphs(): array {
    return [
      'StyleOptionsParagraph' => [
        'field_background_color' => 'paragraph_field_resolver',
        'field_paragraph_layout' => 'paragraph_field_resolver',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getParagraphTypeResolvers(): array {
    return [
      'style_options' => 'StyleOptionsParagraph',
    ];
  }

}
