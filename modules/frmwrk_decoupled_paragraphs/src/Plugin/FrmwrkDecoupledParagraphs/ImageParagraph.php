<?php

namespace Drupal\frmwrk_decoupled_paragraphs\Plugin\FrmwrkDecoupledParagraphs;

use Drupal\frmwrk_decoupled_paragraphs\FrmwrkDecoupledParagraphsPluginBase;

/**
 * Plugin implementation of the frmwrk_decoupled_paragraphs.
 *
 * @FrmwrkDecoupledParagraphs(
 *   id = "image_paragraph",
 *   description = @Translation("Adds image type paragraphs.")
 * )
 *
 * @package Drupal\frmwrk_decoupled_paragraphs\Plugin\FrmwrkDecoupledParagraphs
 */
class ImageParagraph extends FrmwrkDecoupledParagraphsPluginBase {

  /**
   * {@inheritdoc}
   */
  public function addParagraphs(): array {
    // All paragraphs get their id and parent field name added to them so no
    // need to add it again.
    return [
      // Paragraph type (graphql type, must be a defined type).
      'ImageParagraph' => [
        // Drupal field name => graphql resolver plugin.
        // Any valid graphql plugin can be used to resolve it's data.
        'field_additional_images' => 'paragraph_field_resolver',
        'field_category' => [
          'target_id' => 'paragraph_field_resolver',
        ],
        'field_content' => 'paragraph_field_resolver',
        'field_image' => 'paragraph_field_resolver',
        // Array values will use the paragraph_array_field_resolver.
        'field_image_options' => [
          'target_id' => 'paragraph_field_resolver',
          'revision_id' => 'paragraph_field_resolver',
        ],
        'field_style_options' => 'paragraph_paragraph_field_resolver',
        'field_view_mode' => 'paragraph_field_resolver',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getParagraphTypeResolvers(): array {
    return [
      // Drupal paragraph bundle => graphql type.
      "content_image" => "ImageParagraph",
    ];
  }

}
