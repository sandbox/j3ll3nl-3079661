<?php

namespace Drupal\frmwrk_decoupled_paragraphs;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for frmwrk_decoupled_paragraphs plugins.
 */
abstract class FrmwrkDecoupledParagraphsPluginBase extends PluginBase implements FrmwrkDecoupledParagraphsInterface {

  /**
   * {@inheritdoc}
   */
  public function addParagraphs(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getParagraphTypeResolvers(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getGraphqlBaseFile(): string {
    return $this->loadFileDefinition('base');
  }

  /**
   * {@inheritdoc}
   */
  public function getGraphqlExtensionFile(): string {
    return $this->loadFileDefinition('extension');
  }

  /**
   * Returns schema location.
   *
   * @param string $type
   *   The type of file base|extension.
   *
   * @return string
   *   File path.
   */
  protected function loadFileDefinition(string $type) {
    /** @var \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler */
    $moduleHandler = \Drupal::service('module_handler');

    $id = $this->getPluginId();
    $definition = $this->getPluginDefinition();
    $module = $moduleHandler->getModule($definition['provider']);
    return "{$module->getPath()}/graphql/{$id}.{$type}.graphqls";
  }

}
