<?php

namespace Drupal\frmwrk_decoupled_paragraphs\Services;

use Drupal\frmwrk_decoupled\AbstractFieldDetectorService;
use Drupal\frmwrk_decoupled\DetectableFieldInterface;

/**
 * Class ParagraphFieldDetectorService.
 *
 * @package Drupal\frmwrk_decoupled_paragraphs\Services
 */
class ParagraphFieldDetectorService extends AbstractFieldDetectorService implements DetectableFieldInterface {

  /**
   * {@inheritDoc}
   */
  public function getDefaultFieldName(): string {
    return 'field_paragraph';
  }

  /**
   * {@inheritDoc}
   */
  public function getTargetType(): string {
    return 'paragraph';
  }

}
