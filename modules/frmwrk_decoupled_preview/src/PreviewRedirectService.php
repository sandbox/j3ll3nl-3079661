<?php

namespace Drupal\frmwrk_decoupled_preview;

use DateTime;
use Drupal;
use Drupal\Core\Form\FormStateInterface;
use Drupal\simple_oauth\Entities\ClientEntity;
use Drupal\simple_oauth\Entities\ScopeEntity;
use Drupal\user\Entity\Role;
use Exception;
use League\OAuth2\Server\CryptKey;
use League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;

/**
 * Handle node preview button frontend redirect with token.
 *
 * @package Drupal\rexel_decoupled
 */
class PreviewRedirectService {

  /**
   * Repository to generate accesstoken.
   *
   * @var \League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface
   */
  private $accessTokenRepository;

  /**
   * Repository to generate refreshtoken.
   *
   * @var \Drupal\rexel_decoupled\RefreshTokenRepositoryInterface
   */
  private $refreshTokenRepository;

  /**
   * Simple OAuth config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private $simpleOauthConfig;

  /**
   * State interface.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  private $state;

  /**
   * PreviewRedirectService constructor.
   *
   * @param \League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface $accessTokenRepository
   *   Repository to generate accesstoken.
   * @param \Drupal\rexel_decoupled\RefreshTokenRepositoryInterface $refreshTokenRepository
   *   Repository to generate refreshtoken.
   */
  public function __construct(AccessTokenRepositoryInterface $accessTokenRepository, RefreshTokenRepositoryInterface $refreshTokenRepository) {
    $this->accessTokenRepository = $accessTokenRepository;
    $this->refreshTokenRepository = $refreshTokenRepository;
    $this->simpleOauthConfig = \Drupal::config('simple_oauth.settings');
    $this->state = Drupal::state();
  }

  /**
   * Get preview redirect url.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Form state object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Throw something.
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Throw something.
   * @throws \League\OAuth2\Server\Exception\UniqueTokenIdentifierConstraintViolationException
   *   Throw something.
   */
  public function getUrl(FormStateInterface $formState) {
    $accessTokenstring = $this->getAccessToken();

    $globalSiteConfiguration = $this->state->get(str_replace('_', '', ucwords('global_site_configuration_form', '_')));

    $path = '/node/' . $formState->getFormObject()->getEntity()->id();

    $url = $globalSiteConfiguration['frontend_url'] . $path . "?btok=" . $accessTokenstring;

    return $url;
  }

  /**
   * Get access token.
   *
   * @return \Lcobucci\JWT\Token
   *   Token.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Throw something.
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Throw something.
   * @throws \League\OAuth2\Server\Exception\UniqueTokenIdentifierConstraintViolationException
   *   Throw something.
   */
  private function getAccessToken() {
    $consumer = \Drupal::entityTypeManager()->getStorage('consumer')
      ->load(2);

    $client = new ClientEntity($consumer);

    $contentModeratorRole = Role::load('content_moderator');
    $contentModeratorScope = new ScopeEntity($contentModeratorRole);

    $authenticatedRole = Role::load('authenticated');
    $authenticatedScope = new ScopeEntity($authenticatedRole);

    $scopes = [$contentModeratorScope, $authenticatedScope];
    $userIdentifier = 4;

    /** @var \League\OAuth2\Server\Entities\AccessTokenEntityInterface $accessToken */
    $accessToken = $this->accessTokenRepository->getNewToken($client, $scopes, $userIdentifier);

    /** @var \League\OAuth2\Server\Entities\RefreshTokenEntityInterface $refreshToken */
    $refreshToken = $this->refreshTokenRepository->getNewRefreshToken();

    $datetime = new DateTime('+5 minutes');

    $accessToken->setExpiryDateTime($datetime);
    $accessToken->setIdentifier($this->getUuid());

    $refreshToken->setExpiryDateTime($datetime);
    $refreshToken->setAccessToken($accessToken);
    $refreshToken->setIdentifier($this->getUuid());

    $this->accessTokenRepository->persistNewAccessToken($accessToken);
    $this->refreshTokenRepository->persistNewRefreshToken($refreshToken);

    $privateKeyPath = $this->simpleOauthConfig->get('private_key');

    $privateKey = new CryptKey($privateKeyPath);

    return $accessToken->convertToJWT($privateKey);
  }

  /**
   * Get universally unique ID.
   *
   * @param int $length
   *   UUID length.
   *
   * @return string|null
   *   Return UUID.
   */
  private function getUuid($length = 40) {
    try {
      return bin2hex(random_bytes($length));
    }
    catch (Exception $e) {
      // Something went wrong.
      return NULL;
    }
  }

}
