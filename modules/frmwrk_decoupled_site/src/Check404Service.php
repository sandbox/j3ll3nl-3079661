<?php

namespace Drupal\frmwrk_decoupled_site;

use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathMatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\language\ConfigurableLanguageManagerInterface;
use Drupal\redirect_404\RedirectNotFoundStorageInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class Check404Service.
 */
class Check404Service {

  /**
   * Drupal\Core\Path\CurrentPathStack definition.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $pathCurrent;

  /**
   * Drupal\Core\Path\PathMatcherInterface definition.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Drupal\language\ConfigurableLanguageManagerInterface definition.
   *
   * @var \Drupal\language\ConfigurableLanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Drupal\redirect_404\RedirectNotFoundStorageInterface definition.
   *
   * @var \Drupal\redirect_404\RedirectNotFoundStorageInterface
   */
  protected $redirectStorage;

  /**
   * Configuration on 404 settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructs a new Check404Service object.
   */
  public function __construct(CurrentPathStack $path_current, PathMatcherInterface $path_matcher, RequestStack $request_stack, ConfigurableLanguageManagerInterface $language_manager, RedirectNotFoundStorageInterface $redirect_not_found_storage, ConfigFactoryInterface $config_factory) {
    $this->pathCurrent = $path_current;
    $this->pathMatcher = $path_matcher;
    $this->requestStack = $request_stack;
    $this->languageManager = $language_manager;
    $this->redirectStorage = $redirect_not_found_storage;
    $this->config = $config_factory->get('redirect_404.settings');
  }

  /**
   * @param string $path
   */
  public function check(string $path) {
    // Ignore paths specified in the redirect settings.
    if ($pages = mb_strtolower($this->config->get('pages'))) {
      // Do not trim a trailing slash if that is the complete path.
      $path_to_match = $path === '/' ? $path : rtrim($path, '/');

      if ($this->pathMatcher->matchPath(mb_strtolower($path_to_match), $pages)) {
        return;
      }
    }

    // Allow to store paths with arguments.
    if ($query_string = $this->requestStack->getCurrentRequest()
      ->getQueryString()) {
      $query_string = '?' . $query_string;
    }
    $path .= $query_string;
    $langcode = $this->languageManager->getCurrentLanguage()->getId();

    // Write record.
    $this->redirectStorage->logRequest($path, $langcode);
  }

}
