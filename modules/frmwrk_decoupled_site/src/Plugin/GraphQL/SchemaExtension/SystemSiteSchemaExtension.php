<?php

namespace Drupal\frmwrk_decoupled_site\Plugin\GraphQL\SchemaExtension;

use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;

/**
 * Class SystemSiteSchemaExtension.
 *
 * @SchemaExtension(
 *   id = "system_site_extension",
 *   name = "System site extension",
 *   description = "Extend schema with system site information",
 *   schema = "frmwrk"
 * )
 */
class SystemSiteSchemaExtension extends SdlSchemaExtensionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry): void {
    $builder = new ResolverBuilder();

    $this->addConfigs($registry, $builder);
  }

  /**
   * Add config fields.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   Resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   Resolver builder.
   */
  private function addConfigs(ResolverRegistryInterface $registry, ResolverBuilder $builder): void {

    $registry->addFieldResolver('Config', 'systemsite',
      $builder->compose(
        $builder->produce('config_object')
          ->map('configs', $builder->fromParent())
          ->map('name', $builder->fromValue('system.site'))
      )
    );

    $registry->addFieldResolver('SystemSite', 'name',
      $builder->compose(
        $builder->produce('config_value')
          ->map('config', $builder->fromParent())
          ->map('name', $builder->fromValue('name'))
      )
    );

    $registry->addFieldResolver('SystemSite', 'slogan',
      $builder->compose(
        $builder->produce('config_value')
          ->map('config', $builder->fromParent())
          ->map('name', $builder->fromValue('slogan'))
      )
    );
  }

}
