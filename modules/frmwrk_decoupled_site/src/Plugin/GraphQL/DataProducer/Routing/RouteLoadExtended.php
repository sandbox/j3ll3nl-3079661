<?php

namespace Drupal\frmwrk_decoupled_site\Plugin\Graphql\DataProducer\Routing;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\frmwrk_decoupled_site\Check404Service;
use Drupal\graphql\Plugin\GraphQL\DataProducer\Routing\RouteLoad;
use Drupal\redirect\RedirectRepository;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Extended route load.
 *
 * @DataProducer(
 *   id = "route_load_extended",
 *   name = @Translation("Load route with 404 log"),
 *   description = @Translation("Loads a route."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Route")
 *   ),
 *   consumes = {
 *     "path" = @ContextDefinition("string",
 *       label = @Translation("Path")
 *     )
 *   }
 * )
 */
class RouteLoadExtended extends RouteLoad {

  /**
   * Check 404 service.
   *
   * @var \Drupal\frmwrk_decoupled_site\Check404Service
   */
  protected $check404Service;

  /**
   * @var \Drupal\workflows\StateInterface
   */
  private $state;

  /**
   * Create route_load with 404 check service.
   *
   * @inheritDoc
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('path.validator'),
      $container->get('frmwrk_decoupled_site.404'),
      $container->get('state'),
      $container->get('redirect.repository', ContainerInterface::NULL_ON_INVALID_REFERENCE)
    );
  }

  /**
   * RouteLoadExtended constructor.
   *
   * @inheritDoc
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, PathValidatorInterface $pathValidator, Check404Service $check404Service, StateInterface $state, RedirectRepository $redirectRepository = NULL) {
    parent::__construct($configuration, $pluginId, $pluginDefinition, $pathValidator, $redirectRepository);
    $this->check404Service = $check404Service;
    $this->state = $state;
  }

  /**
   * Check on 404 path.
   *
   * @inheritDoc
   */
  public function resolve($path, RefinableCacheableDependencyInterface $metadata) {
    if ((!$path || $path === '/') && $globalSiteConfiguration = $this->state->get('GlobalSiteConfigurationForm')) {
      $path = $globalSiteConfiguration['frontend_base_path'];
    }

    if (!$route = parent::resolve($path, $metadata)) {
      $this->check404Service->check($path);
    }

    return $route;
  }

}
