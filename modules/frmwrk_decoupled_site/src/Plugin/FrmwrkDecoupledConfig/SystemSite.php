<?php

namespace Drupal\frmwrk_decoupled\Plugin\FrmwrkDecoupledConfig;

use Drupal\Component\Plugin\PluginBase;
use Drupal\frmwrk_decoupled\FrmwrkDecoupledConfigInterface;

/**
 * Plugin implementation of the frmwrk_decoupled_config.
 *
 * @FrmwrkDecoupledConfig(
 *   id = "system.site",
 *   configChunk = "system.site"
 * )
 */
class SystemSite extends PluginBase implements FrmwrkDecoupledConfigInterface {

}
