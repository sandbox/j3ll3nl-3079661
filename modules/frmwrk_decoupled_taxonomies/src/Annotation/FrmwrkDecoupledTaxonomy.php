<?php

namespace Drupal\frmwrk_decoupled_taxonomies\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines frmwrk_decoupled_taxonomy annotation object.
 *
 * @Annotation
 */
class FrmwrkDecoupledTaxonomy extends Plugin {

  /**
   * Plugin id.
   *
   * @var string
   */
  public $id;

  /**
   * Taxonomy Vocabulary ID.
   *
   * @var string
   */
  public $vid;

  /**
   * GraphQL implementation for the taxonomy.
   *
   * @var string
   */
  public $resolver;

}
