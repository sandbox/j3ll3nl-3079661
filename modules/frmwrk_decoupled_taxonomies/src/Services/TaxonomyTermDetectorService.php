<?php

namespace Drupal\frmwrk_decoupled_taxonomies\Services;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\frmwrk_decoupled_taxonomies\FrmwrkDecoupledTaxonomyTermsPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TaxonomyTermDetectorService.
 *
 * @package Drupal\frmwrk_decoupled_taxonomies\Services
 */
class TaxonomyTermDetectorService implements TaxonomyTermDetectorServiceInterface {

  const TERMS_CID = "frmwrk_decoupled.terms";
  const VOCAB_CID = "frmwrk_decoupled.vocabularies";


  /**
   * Taxonomy terms plugin manager.
   *
   * @var \Drupal\frmwrk_decoupled_taxonomies\FrmwrkDecoupledTaxonomyTermsPluginManager
   */
  private $taxonomyTermsPluginManager;

  /**
   * Cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private $cacheBackend;

  /**
   * Instantiates a new instance of this class.
   *
   * This is a factory method that returns a new instance of this class. The
   * factory should pass any needed dependencies into the constructor of this
   * class, but not the container itself. Every call to this method must return
   * a new instance of this class; that is, it may not implement a singleton.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container this instance should use.
   *
   * @return \Drupal\frmwrk_decoupled_taxonomies\services\TaxonomyTermDetectorService
   *   Node detector service.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.frmwrk_decoupled_taxonomy_terms'),
      $container->get('cache.default')
    );
  }

  /**
   * NodeDetectorService constructor.
   *
   * @param \Drupal\frmwrk_decoupled_taxonomies\FrmwrkDecoupledTaxonomyTermsPluginManager $taxonomyTermsPluginManager
   *   Taxonomy term plugin manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   Cache backend.
   */
  public function __construct(FrmwrkDecoupledTaxonomyTermsPluginManager $taxonomyTermsPluginManager, CacheBackendInterface $cacheBackend) {
    $this->cacheBackend = $cacheBackend;
    $this->taxonomyTermsPluginManager = $taxonomyTermsPluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getVocabularies(): array {
    if ($vocabularies = $this->cacheBackend->get(self::VOCAB_CID)) {
      return $vocabularies->data;
    }

    $vocabularies = [];

    foreach ($this->taxonomyTermsPluginManager->getDefinitions() as $pluginDefinition) {
      $vocabularies[] = [];
    }

    $this->cacheBackend->set(self::VOCAB_CID, $vocabularies);

    return $vocabularies;
  }

  /**
   * {@inheritdoc}
   */
  public function getTaxonomyTermDefinitions(): array {
    if ($termDefinitions = $this->cacheBackend->get(self::TERMS_CID)) {
      return $termDefinitions->data;
    }
    $termDefinitions = [];
    foreach ($this->taxonomyTermsPluginManager->getDefinitions() as $pluginDefinition) {
      /** @var \Drupal\frmwrk_decoupled_taxonomies\FrmwrkDecoupledTaxonomyTermsInterface $plugin */
      $plugin = $this->taxonomyTermsPluginManager->createInstance($pluginDefinition['id']);
      $termDefinitions[] = $plugin->getGraphqlBaseFile();
    }

    $this->cacheBackend->set(self::TERMS_CID, $termDefinitions);

    return $termDefinitions;
  }

}
