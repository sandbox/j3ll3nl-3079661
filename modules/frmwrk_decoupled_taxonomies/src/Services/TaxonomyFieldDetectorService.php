<?php

namespace Drupal\frmwrk_decoupled_taxonomies\Services;

use Drupal\frmwrk_decoupled\AbstractFieldDetectorService;
use Drupal\frmwrk_decoupled\DetectableFieldInterface;

/**
 * Class ParagraphFieldDetectorService.
 *
 * @package Drupal\frmwrk_decoupled_taxonomies\Services
 */
class TaxonomyFieldDetectorService extends AbstractFieldDetectorService implements DetectableFieldInterface {

  /**
   * {@inheritDoc}
   */
  public function getDefaultFieldName(): string {
    return 'field_taxonomy';
  }

  /**
   * {@inheritDoc}
   */
  public function getTargetType(): string {
    return 'taxonomy_term';
  }

}
