<?php

namespace Drupal\frmwrk_decoupled_taxonomies\Services;

/**
 * Interface TaxonomyTermDetectorServiceInterface.
 *
 * @package Drupal\frmwrk_decoupled_taxonomies\Services
 */
interface TaxonomyTermDetectorServiceInterface {

  /**
   * Return all discovered vocabularies.
   *
   * Results will be cached permanently, upon adding more terms it is expected
   * to do a cache rebuild.
   *
   * @return array
   *   Node types.
   */
  public function getVocabularies(): array;

  /**
   * Return all discovered taxonomy term definitions.
   *
   * Results will be cached permanently, upon adding more terms or vocabularies
   * it is expected to do a cache rebuild.
   *
   * @return array
   *   Collection of graphQL file locations.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getTaxonomyTermDefinitions(): array;

}
