<?php

namespace Drupal\frmwrk_decoupled_taxonomies\Plugin\GraphQL\DataProducer;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\taxonomy\TermInterface;

/**
 * Class TaxonomyFieldResolver.
 *
 * @DataProducer(
 *   id = "taxonomy_field_resolver",
 *   name = @Translation("taxonomy field resolver"),
 *   description = @Translation("Resolve a specific taxonomy field "),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Taxonomy field value")
 *   ),
 *   consumes = {
 *     "entity" = @ContextDefinition("any",
 *       label = @Translation("Taxonomy|Object"),
 *       required = TRUE
 *     ),
 *    "field" = @ContextDefinition("string",
 *       label = @Translation("Field"),
 *       required = TRUE
 *     ),
 *   }
 * )
 *
 * @package Drupal\frmwrk_decoupled\Plugin\GraphQL\DataProducer
 */
class TaxonomyFieldResolver extends DataProducerPluginBase {

  /**
   * Resolves a taxonomy field.
   *
   * @param \Drupal\taxonomy\TermInterface $taxonomy
   *   Taxonomy or Taxonomy field.
   * @param string $field
   *   Taxonomy field to resolve.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $metadata
   *   Cache metadata.
   *
   * @return string|null
   *   Field value or null.
   */
  public function resolve(TermInterface $taxonomy, string $field, RefinableCacheableDependencyInterface $metadata) {
    $metadata->addCacheableDependency($taxonomy);

    if (!$taxonomy->hasField($field)) {
      return NULL;
    }

    return $taxonomy->get($field)->value;
  }

}
