<?php

namespace Drupal\frmwrk_decoupled_taxonomies\Plugin\GraphQL\DataProducer;

use Drupal\Core\Annotation\ContextDefinition;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\frmwrk_decoupled_taxonomies\Services\TaxonomyTermDetectorServiceInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\frmwrk_decoupled\Wrappers\QueryConnection;

/**
 * Class TaxonomyTerms.
 *
 * @DataProducer(
 *   id = "taxonomy_term_field_resolver",
 *   name = @Translation("Taxonomy term reference"),
 *   description = @Translation("Loads a list of taxonomy terms on a node."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Taxonomy terms")
 *   ),
 *   consumes = {
 *     "offset" = @ContextDefinition("string",
 *       label = @Translation("Offset to start the list from"),
 *       required = FALSE
 *     ),
 *     "vid" = @ContextDefinition("string",
 *       label = @Translation("Vocabulary IDs comma seperated"),
 *       required = FALSE
 *     )
 *   }
 * )
 */
class TaxonomyTerms extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  const MAX_LIMIT = 100;

  /**
   * Entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * Taxonomy term detector service.
   *
   * @var \Drupal\frmwrk_decoupled_taxonomies\Services\TaxonomyTermDetectorServiceInterface
   */
  private $termDetectorService;

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.manager'),
      $container->get('frmwrk_decoupled.services.taxonomy_term_detector_service')
    );
  }

  /**
   * Basic pages constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $pluginId
   *   The plugin id.
   * @param mixed $pluginDefinition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityManager
   *   Entity manager.
   * @param \Drupal\frmwrk_decoupled_taxonomies\Services\TaxonomyTermDetectorServiceInterface $termDetectorService
   *   Taxonomy Term detector service.
   *
   * @codeCoverageIgnore
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    EntityTypeManagerInterface $entityManager,
    TaxonomyTermDetectorServiceInterface $termDetectorService
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->entityManager = $entityManager;
    $this->termDetectorService = $termDetectorService;
  }

  /**
   * Node query resolver.
   *
   * @param int $offset
   *   Offset.
   * @param int $limit
   *   Limit.
   * @param string $bundles
   *   Node bundles to filter on.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $metadata
   *   Cache metadata.
   *
   * @return mixed
   *   Query connection.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function resolve() {
    $limit = self::MAX_LIMIT;

    $storage = $this->entityManager->getStorage('taxonomy_term');
    $type = $storage->getEntityType();
    $query = $storage->getQuery()
      ->currentRevision()
      ->accessCheck();

    $query->condition($type->getKey('vid'), explode(',', $bundles), 'IN');
    $query->range($offset, $limit);

    $metadata->addCacheTags($type->getListCacheTags());
    $metadata->addCacheContexts($type->getListCacheContexts());

    return new QueryConnection($query);
  }

}
