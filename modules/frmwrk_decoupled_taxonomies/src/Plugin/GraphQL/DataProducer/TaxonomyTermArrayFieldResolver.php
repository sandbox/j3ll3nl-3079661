<?php

namespace Drupal\frmwrk_decoupled_taxonomies\Plugin\GraphQL\DataProducer;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\taxonomy\TermInterface;

/**
 * Class TaxonomyTermArrayFieldResolver.
 *
 * @DataProducer(
 *   id = "taxonomy_term_array_field_resolver",
 *   name = @Translation("Taxonomy term array field resolver"),
 *   description = @Translation("Resolve a specific taxonomy term array type field "),
 *   produces = @ContextDefinition("list",
 *     label = @Translation("Taxonomy term array field value(s)")
 *   ),
 *   consumes = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("Taxonomy term"),
 *       required = TRUE
 *     ),
 *    "field" = @ContextDefinition("string",
 *       label = @Translation("Field"),
 *       required = TRUE
 *     ),
 *   }
 * )
 *
 * @package Drupal\frmwrk_decoupled_taxonomies\Plugin\GraphQL\DataProducer
 */
class TaxonomyTermArrayFieldResolver extends DataProducerPluginBase {

  /**
   * Resolve taxonomy term array field returning the contained props.
   *
   * @param \Drupal\taxonomy\TermInterface $term
   *   Taxonomy Term.
   * @param string $field
   *   Field to use.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $metadata
   *   Cache metadata.
   *
   * @return array
   *   Taxonomy term field data.
   */
  public function resolve(TermInterface $term, string $field, RefinableCacheableDependencyInterface $metadata): array {
    return $term->hasField($field) ? $term->get($field)->getValue() : [];
  }

}
