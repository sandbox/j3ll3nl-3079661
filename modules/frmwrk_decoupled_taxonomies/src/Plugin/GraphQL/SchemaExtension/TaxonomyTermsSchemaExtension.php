<?php

namespace Drupal\frmwrk_decoupled_taxonomies\Plugin\GraphQL\SchemaExtension;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\frmwrk_decoupled\Services\NodeDetectorServiceInterface;
use Drupal\frmwrk_decoupled_taxonomies\FrmwrkDecoupledTaxonomyTermsPluginManager;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;
use Drupal\taxonomy\TermInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TaxonomyTermsSchemaExtension.
 *
 * @SchemaExtension(
 *   id = "taxonomy_extension",
 *   name = "Taxonomy term extension",
 *   description = "Extend schema with node taxonomies and corresponding fields",
 *   schema = "frmwrk"
 * )
 *
 * @package Drupal\frmwrk_decoupled_taxonomies\Plugin\GraphQL\SchemaExtension
 */
class TaxonomyTermsSchemaExtension extends SdlSchemaExtensionPluginBase {

  use StringTranslationTrait;

  /**
   * Array of taxonomy vocabularies.
   *
   * @var array
   */
  private $vocabularies = [];

  /**
   * Drupal vocabularies and their graphql resolver.
   *
   * @var array
   */
  private $vocabularyResolvers = [
    'NotYetImplementedTaxonomy' => 'NotYetImplementedTaxonomy',
  ];

  /**
   * GraphQL base files defined by taxonomy term extensions.
   *
   * @var array
   */
  private $gqlBaseFiles = [];

  /**
   * GraphQL extension files defined by taxonomy term extensions.
   *
   * @var array
   */
  private $gqlExtensionFiles = [];

  /**
   * Node detector service.
   *
   * @var \Drupal\frmwrk_decoupled\services\NodeDetectorServiceInterface
   */
  private $nodeDetectorService;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function __construct(
    $configuration,
    $pluginId,
    $pluginDefinition,
    ModuleHandlerInterface $moduleHandler,
    FrmwrkDecoupledTaxonomyTermsPluginManager $pluginManager,
    NodeDetectorServiceInterface $nodeDetectorService
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition, $moduleHandler);

    $this->nodeDetectorService = $nodeDetectorService;

    foreach ($pluginManager->getDefinitions() as $pluginDefinition) {
      /** @var \Drupal\frmwrk_decoupled_taxonomies\FrmwrkDecoupledTaxonomyTermsInterface $plugin */
      $plugin = $pluginManager->createInstance($pluginDefinition['id']);

      $this->vocabularies = array_merge($this->vocabularies, $plugin->addTaxonomyTerms());
      $this->vocabularyResolvers = array_merge($this->vocabularyResolvers, $plugin->getVocabularyResolvers());

      $this->gqlBaseFiles[] = $plugin->getGraphqlBaseFile();
      $this->gqlExtensionFiles[] = $plugin->getGraphqlExtensionFile();
    }
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): TaxonomyTermsSchemaExtension {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('plugin.manager.frmwrk_decoupled_taxonomy_terms'),
      $container->get('frmwrk_decoupled.services.node_detector_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry): void {
    $builder = new ResolverBuilder();

    foreach ($this->nodeDetectorService->getNodeTypes() as $type) {
      $this->addTaxonomyTermResolver($registry);
      $this->addVocabularies($type, $registry, $builder);
    }

    foreach ($this->vocabularies as $type => $fields) {
      // Taxonomy term default fields, every vocabulary must implement.
      $this->addTaxonomyTermFields($type, [
        'vid' => 'entity_id',
      ], $registry, $builder);

      $this->addTaxonomyTermFields($type, $fields, $registry, $builder);
    }
  }

  /**
   * Add interfaces.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   Resolver registry.
   */
  private function addTaxonomyTermResolver(ResolverRegistryInterface $registry): void {
    $registry->addTypeResolver('TaxonomyTermInterface', function ($value) {
      if ($value instanceof TermInterface) {
        return $this->taxonomytermVocabularies[$value->bundle()] ?? "TaxonomyTerm";
      }

      throw new \Exception($this->t("Could not resolve vocabulary."));
    });
  }

  /**
   * Add vocabularies.
   *
   * @param string $type
   *   Node type.
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   Resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   Resolver builder.
   */
  private function addVocabularies(string $type, ResolverRegistryInterface $registry, ResolverBuilder $builder): void {
    // Add taxonomies to nodes consuming the node.
    $registry->addFieldResolver($type, 'assortment',
      $builder->produce('taxonomy_term_field_resolver')
        ->map('offset', $builder->fromArgument('offset'))
        ->map('vid', $builder->fromArgument('vid'))
    );
  }

  /**
   * Add fields to taxonomies.
   *
   * @param string $parent
   *   Parent field.
   * @param array $fields
   *   Fields to be added under the defined parent field.
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   Resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   Resolver builder.
   */
  public function addTaxonomyTermFields(string $parent, array $fields, ResolverRegistryInterface $registry, ResolverBuilder $builder): void {
    foreach ($fields as $field => $resolver) {
      if (is_array($resolver)) {
        // Recursive call to add the array's children.
        $this->addTaxonomyTermFields($field, $resolver, $registry, $builder);

        // Set the resolver to use the array resolver.
        $resolver = 'taxonomy_term_array_field_resolver';
      }

      // Map the graphql field to the drupal fields.
      $registry->addFieldResolver($parent, $field,
        $builder->produce($resolver)
          ->map('entity', $builder->fromParent())
          ->map('field', $builder->fromValue($field))
      );
    }
  }

}
