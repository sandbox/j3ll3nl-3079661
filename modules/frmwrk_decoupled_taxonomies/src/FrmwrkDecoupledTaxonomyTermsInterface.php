<?php

namespace Drupal\frmwrk_decoupled_taxonomies;

/**
 * Interface for frmwrk_decoupled_taxonomy plugins.
 */
interface FrmwrkDecoupledTaxonomyTermsInterface {

  /**
   * Returns the fields to be added to the taxonomy term section of a node.
   *
   * @return array
   *   Terms with their fields.
   */
  public function addTaxonomyTerms(): array;

  /**
   * Return the graphql resolver for a taxonomy vocabulary.
   *
   * @return array
   *   Vocabularies with respective resolver.
   */
  public function getVocabularyResolvers(): array;

  /**
   * Return the file location for the base .graphqls file.
   *
   * The path must not contain the file extension.
   *
   * @return string
   *   GraphQL base location.
   */
  public function getGraphqlBaseFile(): string;

  /**
   * Return the file location for the extension .graphqls file.
   *
   * The path must not contain the file extension.
   *
   * @return string
   *   GraphQL extension location.
   */
  public function getGraphqlExtensionFile(): string;

}
