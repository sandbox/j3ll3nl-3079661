<?php

namespace Drupal\frmwrk_decoupled_metatags\Plugin\GraphQL\SchemaExtension;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\frmwrk_decoupled\services\NodeDetectorServiceInterface;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MetaTagsSchemaExtension.
 *
 * @SchemaExtension(
 *   id = "metatag_extension",
 *   name = "Metatag extension",
 *   description = "Extend schema with node metatags",
 *   schema = "frmwrk"
 * )
 *
 * @package Drupal\frmwrk_decoupled_metatags\Plugin\GraphQL\SchemaExtension
 */
class MetatagsSchemaExtension extends SdlSchemaExtensionPluginBase {

  /**
   * Node detector service.
   *
   * @var \Drupal\frmwrk_decoupled\services\NodeDetectorServiceInterface
   */
  private $nodeDetectorService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('frmwrk_decoupled.services.node_detector_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct($configuration, $pluginId, $pluginDefinition, ModuleHandlerInterface $moduleHandler, NodeDetectorServiceInterface $nodeDetectorService) {
    parent::__construct($configuration, $pluginId, $pluginDefinition, $moduleHandler);

    $this->nodeDetectorService = $nodeDetectorService;
  }

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry): void {
    $builder = new ResolverBuilder();

    $this->addMetatags($registry, $builder);
  }

  /**
   * Add meta tag fields.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   Resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   Resolver builder.
   */
  private function addMetatags(ResolverRegistryInterface $registry, ResolverBuilder $builder): void {
    foreach ($this->nodeDetectorService->getNodeTypes() as $type) {
      $registry->addFieldResolver($type, 'metatags',
        $builder->compose(
          $builder->produce('metatags')
            ->map('entity', $builder->fromParent())
        )
      );

      $registry->addFieldResolver('metatag', 'name',
        $builder->compose(
          $builder->produce('metatag_name')
            ->map('tag', $builder->fromParent())
        )
      );
      $registry->addFieldResolver('metatag', 'content',
        $builder->compose(
          $builder->produce('metatag_content')
            ->map('tag', $builder->fromParent())
        )
      );
    }
  }

}
