<?php

namespace Drupal\frmwrk_decoupled_metatags\Plugin\GraphQL\DataProducer;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\metatag\MetatagManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MetatagName.
 *
 * @DataProducer(
 *   id = "metatag_name",
 *   name = @Translation("Metatag name parser"),
 *   description = @Translation("Parses the metatag's name"),
 *   produces = @ContextDefinition("string",
 *     label = @Translation("Metatag name")
 *   ),
 *   consumes = {
 *     "tag" = @ContextDefinition("string",
 *       label = @Translation("Metatags"),
 *       required = TRUE
 *     ),
 *   }
 * )
 *
 * @package Drupal\frmwrk_decoupled_metatags\Plugin\GraphQL\DataProducer
 */
class MetatagName extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Metatag manager.
   *
   * @var \Drupal\metatag\MetatagManagerInterface
   */
  private $metatagManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): MetatagName {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('metatag.manager')
    );
  }

  /**
   * Metatags constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $pluginId
   *   The plugin id.
   * @param mixed $pluginDefinition
   *   The plugin definition.
   * @param \Drupal\metatag\MetatagManagerInterface $metatagManager
   *   Metatag manager.
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, MetatagManagerInterface $metatagManager) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);

    $this->metatagManager = $metatagManager;
  }

  /**
   * Config object resolver.
   *
   * @param mixed $tag
   *   Content type identifier.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $metadata
   *   Cache metadata.
   *
   * @return mixed
   *   Config value.
   */
  public function resolve($tag, RefinableCacheableDependencyInterface $metadata): string {
    return $tag['name'];
  }

}
