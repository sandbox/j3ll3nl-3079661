<?php

namespace Drupal\frmwrk_decoupled_metatags\Plugin\GraphQL\DataProducer;

use Drupal\Component\Render\PlainTextOutput;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\metatag\MetatagManagerInterface;
use Drupal\metatag\MetatagToken;
use Drupal\views\ViewEntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MetaTags.
 *
 * @DataProducer(
 *   id = "metatags",
 *   name = @Translation("Load an entities metatags"),
 *   description = @Translation("Load an entities metatags"),
 *   produces = @ContextDefinition("list",
 *     label = @Translation("Metatags")
 *   ),
 *   consumes = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("Entity"),
 *       required = TRUE
 *     ),
 *   }
 * )
 */
class Metatags extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Metatag manager.
   *
   * @var \Drupal\metatag\MetatagManagerInterface
   */
  private $metatagManager;

  /**
   * @var \Drupal\metatag\MetatagToken
   */
  private $tokenService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): Metatags {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('metatag.manager'),
      $container->get('metatag.token')
    );
  }

  /**
   * Metatags constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $pluginId
   *   The plugin id.
   * @param mixed $pluginDefinition
   *   The plugin definition.
   * @param \Drupal\metatag\MetatagManagerInterface $metatagManager
   *   Metatag manager.
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, MetatagManagerInterface $metatagManager, MetatagToken $token) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);

    $this->metatagManager = $metatagManager;
    $this->tokenService = $token;
  }

  /**
   * Metatags resolver.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Content type identifier.
   *
   * @return mixed
   *   Node metatags.
   */
  public function resolve(ContentEntityInterface $entity, RefinableCacheableDependencyInterface $metadata): array {
    $metadata->addCacheableDependency($entity);

    $unprocessedMetatags = array_merge($this->metatagManager->defaultTagsFromEntity($entity), $this->metatagManager->tagsFromEntity($entity));

    if (isset($unprocessedMetatags['canonical_url'])) {
      $options['absolute'] = TRUE;
      $url = \Drupal::urlGenerator()->generateFromRoute('entity.node.canonical', ['node' => $entity->id()], $options, TRUE);

      $unprocessedMetatags['canonical_url'] = $url->getGeneratedUrl();
      $metadata->addCacheableDependency($url);
    }

    $token_replacements = [];
    if ($entity) {
      // @todo This needs a better way of discovering the context.
      if ($entity instanceof ViewEntityInterface) {
        // Views tokens require the ViewExecutable, not the config entity.
        // @todo Can we move this into metatag_views somehow?
        $token_replacements = ['view' => $entity->getExecutable()];
      }
      elseif ($entity instanceof ContentEntityInterface) {
        $token_replacements = [$entity->getEntityTypeId() => $entity];
      }
    }

    $bubbleMetadata = new BubbleableMetadata();

    foreach ($unprocessedMetatags as $name => $value) {

      $langcode = \Drupal::languageManager()->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();

      $processed_value = PlainTextOutput::renderFromHtml(htmlspecialchars_decode($this->tokenService->replace($value, $token_replacements, ['langcode' => $langcode], $bubbleMetadata)));

      $metatags[] = [
        'name' => $name,
        'content' => $processed_value,
      ];
    }

    $metadata->addCacheableDependency($bubbleMetadata);

    return $metatags;
  }

}
