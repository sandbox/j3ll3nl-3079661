<?php

namespace Drupal\frmwrk_decoupled_media\Services;

/**
 * Class MediaFieldDetectorService.
 *
 * @package Drupal\frmwrk_decoupled_media\Services
 */
interface MediaFieldDetectorServiceInterface {

  /**
   * Get media field names.
   *
   * @return string|array
   *   Field names on the node and bundle with the media type.
   */
  public function getFieldNames();

  /**
   * Return media fields for a given bundle.
   *
   * @param string $bundle
   *   Bundle.
   *
   * @return array|false
   *   Field name or false.
   */
  public function getFieldNamesByBundle(string $bundle);

}
