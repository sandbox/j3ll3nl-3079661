<?php

namespace Drupal\frmwrk_decoupled_media\Services;

/**
 * Interface MediaDetectorServiceInterface.
 *
 * @package Drupal\frmwrk_decoupled_media\services
 */
interface MediaDetectorServiceInterface {

  /**
   * Return all discovered media definitions.
   *
   * Results will be cached permanently, upon adding more medias it is expected
   * to do a cache rebuild.
   *
   * @return array
   *   Collection of graphQL file locations.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getMediaDefinitions(): array;

}
