<?php

namespace Drupal\frmwrk_decoupled_media\Services;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\frmwrk_decoupled\services\NodeDetectorServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MediaFieldDetectorService.
 *
 * @package Drupal\frmwrk_decoupled_media\Services
 */
class MediaFieldDetectorService implements MediaFieldDetectorServiceInterface, ContainerInjectionInterface {

  /**
   * Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  private $entityFieldManager;

  /**
   * Node detector service.
   *
   * @var \Drupal\frmwrk_decoupled\services\NodeDetectorServiceInterface
   */
  private $nodeDetectorService;

  /**
   * MediaFieldDetectorService constructor.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   Entity manager.
   * @param \Drupal\frmwrk_decoupled\services\NodeDetectorServiceInterface $nodeDetectorService
   *   Node detector service.
   */
  public function __construct(EntityFieldManagerInterface $entityFieldManager, NodeDetectorServiceInterface $nodeDetectorService) {
    $this->entityFieldManager = $entityFieldManager;
    $this->nodeDetectorService = $nodeDetectorService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_field.manager'),
      $container->get('frmwrk_decoupled.services.node_detector_service')
    );
  }

  /**
   * Transforms a PascalCase string to snake_case.
   *
   * Useful to transform GraphQL camel/pascal-case fields to drupal machine
   * names.
   *
   * @param string $string
   *   Unformatted camel/Pascal-Case string input.
   *
   * @return string
   *   Formatted string in snake_case.
   */
  public static function pascalToSnake(string $string): string {
    return strtolower(preg_replace(['/([a-z\d])([A-Z])/', '/([^_])([A-Z][a-z])/'], '$1_$2', $string));
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldNames(): array {
    $fieldNames = [];
    foreach ($this->nodeDetectorService->getNodeTypes() as $bundle) {
      if ($fields = $this->getFieldNamesByBundle($bundle)) {
        $fieldNames = array_merge($fieldNames, $fields);
      }
    }

    return isset($fieldNames) ? array_unique($fieldNames) : ['field_media'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldNamesByBundle(string $bundle) {
    $fields = $this->entityFieldManager->getFieldDefinitions('node', self::pascalToSnake($bundle));
    foreach ($fields as $fieldDefinition) {
      if (
        $fieldDefinition->getSetting('target_type')
        && $fieldDefinition->getSetting('target_type') == 'media'
      ) {
        $out[] = $fieldDefinition->get('field_name');
      }
    }

    return $out ?? FALSE;
  }

}
