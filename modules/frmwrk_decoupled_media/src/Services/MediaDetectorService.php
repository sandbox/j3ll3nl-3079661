<?php

namespace Drupal\frmwrk_decoupled_media\Services;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\frmwrk_decoupled_media\FrmwrkDecoupledMediaPluginManager;

/**
 * Class MediaDetectorService.
 *
 * @package Drupal\frmwrk_decoupled_media\services
 */
class MediaDetectorService implements MediaDetectorServiceInterface {

  const MEDIA_CID = "frmwrk_decoupled.media_types";
  const MEDIA_DEF_CID = "frmwrk_decoupled.media_definitions";

  /**
   * Media plugin manager.
   *
   * @var \Drupal\frmwrk_decoupled_media\FrmwrkDecoupledMediaPluginManager
   */
  private $mediaPluginManager;

  /**
   * Cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private $cacheBackend;

  /**
   * MediaDetectorService constructor.
   *
   * @param \Drupal\frmwrk_decoupled_media\FrmwrkDecoupledMediaPluginManager $mediaPluginManager
   *   Media plugin manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   Cache backend.
   */
  public function __construct(FrmwrkDecoupledMediaPluginManager $mediaPluginManager, CacheBackendInterface $cacheBackend) {
    $this->mediaPluginManager = $mediaPluginManager;
    $this->cacheBackend = $cacheBackend;
  }

  /**
   * {@inheritdoc}
   */
  public function getMediaDefinitions(): array {
    if ($mediaDefinitions = $this->cacheBackend->get(self::MEDIA_DEF_CID)) {
      return $mediaDefinitions->data;
    }
    $mediaDefinitions = [];
    foreach ($this->mediaPluginManager->getDefinitions() as $pluginDefinition) {
      /** @var \Drupal\frmwrk_decoupled_media\FrmwrkDecoupledMediaInterface $plugin */
      $plugin = $this->mediaPluginManager->createInstance($pluginDefinition['id']);
      $mediaDefinitions[] = $plugin->getGraphqlBaseFile();
    }

    $this->cacheBackend->set(self::MEDIA_DEF_CID, $mediaDefinitions);

    return $mediaDefinitions;
  }

}
