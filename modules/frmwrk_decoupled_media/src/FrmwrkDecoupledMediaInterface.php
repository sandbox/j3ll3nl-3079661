<?php

namespace Drupal\frmwrk_decoupled_media;

/**
 * Interface for frmwrk_decoupled_media plugins.
 */
interface FrmwrkDecoupledMediaInterface {

  /**
   * Returns the fields to be added to a media entity.
   *
   * @return array
   *   Media types with their fields.
   */
  public function addMedia(): array;

  /**
   * Return the graphql resolver for a media bundle.
   *
   * @return array
   *   Media types with respective resolver.
   */
  public function getMediaTypeResolvers(): array;

  /**
   * Return the file location for the base .graphqls file.
   *
   * The path must not contain the file extension.
   *
   * @return string
   *   GraphQL base location.
   */
  public function getGraphqlBaseFile(): string;

}
