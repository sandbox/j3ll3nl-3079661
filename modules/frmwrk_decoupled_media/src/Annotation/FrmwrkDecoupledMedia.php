<?php

namespace Drupal\frmwrk_decoupled_media\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines frmwrk_decoupled_nodes annotation object.
 *
 * @Annotation
 */
class FrmwrkDecoupledMedia extends Plugin {

  /**
   * Plugin id.
   *
   * @var string
   */
  public $id;

  /**
   * GraphQL implementation for the node.
   *
   * @var string
   */
  public $resolver;

}
