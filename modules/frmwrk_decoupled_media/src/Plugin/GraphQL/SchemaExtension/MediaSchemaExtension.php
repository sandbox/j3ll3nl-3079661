<?php

namespace Drupal\frmwrk_decoupled_media\Plugin\GraphQL\SchemaExtension;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\frmwrk_decoupled\services\NodeDetectorServiceInterface;
use Drupal\frmwrk_decoupled_media\FrmwrkDecoupledMediaPluginManager;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;
use Drupal\media\MediaInterface;
use GraphQL\Error\Error;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MediaSchemaExtension.
 *
 * @SchemaExtension(
 *   id = "media_extension",
 *   name = "Media extension",
 *   description = "Extend schema with node pragraphs and corresponding fields",
 *   schema = "frmwrk"
 * )
 *
 * @package Drupal\frmwrk_decoupled_media\Plugin\GraphQL\SchemaExtension
 */
class MediaSchemaExtension extends SdlSchemaExtensionPluginBase {

  use StringTranslationTrait;

  /**
   * Media type with child values and resolver plugins.
   *
   * @var array
   */
  private $mediaTypes = [
    'NotYetImplementedMedia' => [
      'bundle' => 'media_bundle_resolver',
    ],
  ];

  /**
   * Drupal media types and their graphql resolver.
   *
   * @var array
   */
  private $mediaTypeResolvers = [
    'NotYetImplementedMedia' => 'NotYetImplementedMedia',
  ];

  /**
   * GraphQL base files defined by media extensions.
   *
   * @var array
   */
  private $gqlBaseFiles = [];

  /**
   * Node detector service.
   *
   * @var \Drupal\frmwrk_decoupled\services\NodeDetectorServiceInterface
   */
  private $nodeDetectorService;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function __construct(
    $configuration,
    $pluginId,
    $pluginDefinition,
    ModuleHandlerInterface $moduleHandler,
    FrmwrkDecoupledMediaPluginManager $pluginManager,
    NodeDetectorServiceInterface $nodeDetectorService
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition, $moduleHandler);

    $this->nodeDetectorService = $nodeDetectorService;

    foreach ($pluginManager->getDefinitions() as $pluginDefinition) {
      /** @var \Drupal\frmwrk_decoupled_media\FrmwrkDecoupledMediaInterface $plugin */
      $plugin = $pluginManager->createInstance($pluginDefinition['id']);

      $this->mediaTypes = array_merge($this->mediaTypes, $plugin->addMedia());
      $this->mediaTypeResolvers = array_merge($this->mediaTypeResolvers, $plugin->getMediaTypeResolvers());

      $this->gqlBaseFiles[] = $plugin->getGraphqlBaseFile();
    }

    // Optionally dispatch an event here to make it possible to update some
    // of the values of $this->mediaTypes && $this->mediaTypeResolvers.
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition):MediaSchemaExtension {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('module_handler'),
      $container->get('plugin.manager.frmwrk_decoupled_media'),
      $container->get('frmwrk_decoupled.services.node_detector_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry): void {
    $builder = new ResolverBuilder();

    $registry->addFieldResolver('Image', 'variant',
      $builder->produce('media_image_derivative_simplifier_resolver')
        ->map('fileArray',
          $builder->produce('image_derivative')
            ->map('entity', $builder->fromParent())
            ->map('style', $builder->fromArgument('style'))
        )
    );

    $registry->addFieldResolver('File', 'filename',
      $builder->produce('property_path')
        ->map('value', $builder->produce('media_file_resolver')
          ->map('media', $builder->fromParent())
        )
        ->map('path', $builder->fromValue('filename'))
    );

    $registry->addFieldResolver('Image', 'id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    foreach ($this->nodeDetectorService->getNodeTypes() as $type) {
      $this->addMediaTypeResolver($registry);
      $this->addMedia($type, $registry, $builder);
    }

    foreach ($this->mediaTypes as $type => $fields) {
      // Media default fields, every media type must implement.
      $this->addMediaFields($type, [
        'parent_field_name' => 'media_field_resolver',
        'id' => 'entity_id',
      ], $registry, $builder);

      // Custom media fields defined in self::MEDIA_TYPES.
      $this->addMediaFields($type, $fields, $registry, $builder);
    }
  }

  /**
   * Add interfaces.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   Resolver registry.
   */
  private function addMediaTypeResolver(ResolverRegistryInterface $registry): void {
    $registry->addTypeResolver('MediaTypeInterface', function ($entity) {
      if ($entity instanceof MediaInterface) {
        // Returns the graphql implementation for a media type or the
        // default if none is found.
        return $this->mediaTypeResolvers[$entity->bundle()] ?? 'NotYetImplementedMedia';
      }

      throw new Error($this->t('Could not resolve media type.'));
    });
  }

  /**
   * Add media.
   *
   * @param string $type
   *   Node type.
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   Resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   Resolver builder.
   */
  private function addMedia(string $type, ResolverRegistryInterface $registry, ResolverBuilder $builder): void {
    // Add media to nodes consuming the node.
    $registry->addFieldResolver($type, 'media',
      $builder->produce('media')
        ->map('entity', $builder->fromParent())
    );
  }

  /**
   * Add fields to media.
   *
   * @param string $parent
   *   Parent field.
   * @param array $fields
   *   Fields to be added under the defined parent field.
   * @param \Drupal\graphql\GraphQL\ResolverRegistryInterface $registry
   *   Resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   Resolver builder.
   */
  public function addMediaFields(string $parent, array $fields, ResolverRegistryInterface $registry, ResolverBuilder $builder): void {
    foreach ($fields as $field => $resolver) {
      if (is_array($resolver)) {
        // Call this function again to add the array's children.
        $this->addMediaFields($field, $resolver, $registry, $builder);

        // Set the resolver to use the array resolver.
        $resolver = 'media_array_field_resolver';
      }

      // Map the graphql field to the drupal fields.
      $registry->addFieldResolver($parent, $field,
        $builder->produce($resolver)
          ->map('entity', $builder->fromParent())
          ->map('field', $builder->fromValue($field))
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseDefinition() {
    return $this->appendFiles(parent::getBaseDefinition(), $this->gqlBaseFiles);
  }

  /**
   * Append specified files to the graphQL file.
   *
   * @param string $file
   *   Media gql definition.
   * @param array $locations
   *   File locations defined by plugins.
   *
   * @return string
   *   Graphql.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  private function appendFiles(string $file, array $locations) {
    foreach ($locations as $gqlFile) {
      if (!file_exists($gqlFile)) {
        throw new InvalidPluginDefinitionException(sprintf("Missing schema definition file at %s.", $gqlFile));
      }

      $file .= "\n" . file_get_contents($gqlFile);
    }

    return $file;
  }

}
