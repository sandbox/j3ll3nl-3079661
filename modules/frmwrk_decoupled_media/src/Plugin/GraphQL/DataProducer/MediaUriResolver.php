<?php

namespace Drupal\frmwrk_decoupled_media\Plugin\GraphQL\DataProducer;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\file\Entity\File;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MediaUriResolver.
 *
 * @DataProducer(
 *   id = "media_uri_resolver",
 *   name = @Translation("media uri resolver"),
 *   description = @Translation("Resolve a media item's uri"),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Media uri")
 *   ),
 *   consumes = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("File"),
 *       required = TRUE
 *     ),
 *    "field" = @ContextDefinition("string",
 *       label = @Translation("Field"),
 *       required = TRUE
 *     ),
 *   }
 * )
 *
 * @package Drupal\frmwrk_decoupled_media\Plugin\GraphQL\DataProducer
 */
class MediaUriResolver extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Stream wrapper.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  private $streamWrapperManager;

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ): MediaUriResolver {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('stream_wrapper_manager')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, StreamWrapperManagerInterface $streamWrapperManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->streamWrapperManager = $streamWrapperManager;
  }

  /**
   * Resolves a uri.
   *
   * @param \Drupal\file\Entity\File $entity
   *   Media or media field.
   * @param string $field
   *   Media field to resolve.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $metadata
   *   Cache metadata.
   *
   * @return string|null
   *   Field value or null.
   */
  public function resolve(File $entity, string $field, RefinableCacheableDependencyInterface $metadata) {
    $metadata->addCacheableDependency($entity);

    $internalWrappers = array_keys($this->streamWrapperManager->getWrappers());
    $streamWrapper = explode(':', $entity->getFileUri())[0];

    if (!in_array($streamWrapper, array_keys($this->streamWrapperManager->getWrappers()))) {
      return $entity->getFileUri();
    }

    return $this->streamWrapperManager->getViaUri($entity->getFileUri())->getExternalUrl() ?? NULL;
  }

}
