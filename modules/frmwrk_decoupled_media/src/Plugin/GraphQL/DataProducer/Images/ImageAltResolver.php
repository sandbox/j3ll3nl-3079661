<?php

namespace Drupal\frmwrk_decoupled_media\Plugin\GraphQL\DataProducer\Images;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\file\Entity\File;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\media\Entity\Media;

/**
 * Class ImageAltResolver.
 *
 * @DataProducer(
 *   id = "image_alt_resolver",
 *   name = @Translation("Image alt resolver"),
 *   description = @Translation("Resolve the image alt."),
 *   produces = @ContextDefinition("string",
 *     label = @Translation("Image alt")
 *   ),
 *   consumes = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("File"),
 *       required = TRUE
 *     ),
 *    "field" = @ContextDefinition("string",
 *       label = @Translation("Field"),
 *       required = TRUE
 *     ),
 *   }
 * )
 *
 * @package Drupal\frmwrk_decoupled_media\Plugin\GraphQL\DataProducer\Image
 */
class ImageAltResolver extends DataProducerPluginBase {

  /**
   * Resolves a media field.
   *
   * @param \Drupal\file\Entity\File $entity
   *   Media or media field.
   * @param string $field
   *   Media field to resolve.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $metadata
   *   Cache metadata.
   *
   * @return string|null
   *   Field value or null.
   */
  public function resolve(File $entity, string $field, RefinableCacheableDependencyInterface $metadata) {
    if (!$mediaEntity = Media::load($entity->id())) {
      return NULL;
    }

    $metadata->addCacheableDependency($entity);
    $metadata->addCacheableDependency($mediaEntity);

    return $mediaEntity->get('field_media_image')->alt ?? NULL;
  }

}
