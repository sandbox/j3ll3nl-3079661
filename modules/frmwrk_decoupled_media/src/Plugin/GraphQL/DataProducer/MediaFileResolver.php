<?php

namespace Drupal\frmwrk_decoupled_media\Plugin\GraphQL\DataProducer;

use Drupal\file\Entity\File;
use Drupal\frmwrk_decoupled\Plugin\GraphQL\DataProducer\EntityReflectionBaseResolver;

/**
 * Class MediaFileResolver.
 *
 * @DataProducer(
 *   id = "media_file_resolver",
 *   name = @Translation("media file"),
 *   description = @Translation("Return a media's file"),
 *   produces = @ContextDefinition("list",
 *     label = @Translation("Media file object")
 *   ),
 *   consumes = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("Paragraph"),
 *       required = TRUE
 *     ),
 *    "field" = @ContextDefinition("string",
 *       label = @Translation("Field"),
 *       required = TRUE
 *     ),
 *   }
 * )
 *
 * @package Drupal\frmwrk_decoupled_media\Plugin\GraphQL\DataProducer
 */
class MediaFileResolver extends EntityReflectionBaseResolver {

  /**
   * {@inheritDoc}
   */
  protected function getEntity(): string {
    return 'media';
  }

  /**
   * {@inheritDoc}
   */
  protected function reflectEntity(array $entityReference) {
    $entity = $this->getEntity();
    $entityStorage = $this->entityTypeManager->getStorage($entity);

    /** @var \Drupal\media\MediaInterface $mediaItem */
    $mediaItem = $entityStorage->load($entityReference['target_id']);

    switch ($mediaItem->bundle()) {
      case 'image':
      case 'background_image':
        $field = 'field_media_image';
        break;

      case 'file':
      case 'document':
        $field = 'field_media_file';
        break;

      case 'video':
        $field = 'field_media_video_file';
        break;

      case 'remote_video':
        // When dealing with a remote video we don't have a file object so we
        // forge one.
        $video = new File([], 'file');
        $video->setFilename($mediaItem->get('name')->value);
        $video->setFileUri($mediaItem->get('field_media_oembed_video')->value);

        return $video;

      default:
        break;
    }

    $result = $mediaItem->get($field)->referencedEntities();

    return reset($result);
  }

}
