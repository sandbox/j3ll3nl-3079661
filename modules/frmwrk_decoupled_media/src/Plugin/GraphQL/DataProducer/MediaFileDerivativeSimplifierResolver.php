<?php

namespace Drupal\frmwrk_decoupled_media\Plugin\GraphQL\DataProducer;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;

/**
 * Class MediaFileResolver.
 *
 * @DataProducer(
 *   id = "media_image_derivative_simplifier_resolver",
 *   name = @Translation("media file"),
 *   description = @Translation("Return a media's file"),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Media file object")
 *   ),
 *   consumes = {
 *     "fileArray" = @ContextDefinition("any",
 *       label = @Translation("Media"),
 *       required = TRUE
 *     ),
 *   }
 * )
 *
 * @package Drupal\frmwrk_decoupled_media\Plugin\GraphQL\DataProducer
 */
class MediaFileDerivativeSimplifierResolver extends DataProducerPluginBase {

  /**
   * Resolves a file url from the file array.
   *
   * @param array $fileArray
   *   Drupal file array.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $metadata
   *   Cache metadata.
   *
   * @return string|null
   *   Field value or null.
   */
  public function resolve(array $fileArray, RefinableCacheableDependencyInterface $metadata) {
    return $fileArray['url'];
  }

}
