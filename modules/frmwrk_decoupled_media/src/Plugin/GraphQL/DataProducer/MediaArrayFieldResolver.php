<?php

namespace Drupal\frmwrk_decoupled_media\Plugin\GraphQL\DataProducer;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\media\MediaInterface;

/**
 * Class MediaArrayFieldResolver.
 *
 * @DataProducer(
 *   id = "media_array_field_resolver",
 *   name = @Translation("media array field resolver"),
 *   description = @Translation("Resolve a specific media array type field "),
 *   produces = @ContextDefinition("list",
 *     label = @Translation("Media array field value(s)")
 *   ),
 *   consumes = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("Media"),
 *       required = TRUE
 *     ),
 *    "field" = @ContextDefinition("string",
 *       label = @Translation("Field"),
 *       required = TRUE
 *     ),
 *   }
 * )
 *
 * @package Drupal\frmwrk_decoupled_media\Plugin\GraphQL\DataProducer
 */
class MediaArrayFieldResolver extends DataProducerPluginBase {

  /**
   * Resolve media array field returning the contained props.
   *
   * @param \Drupal\media\MediaInterface $media
   *   Media.
   * @param string $field
   *   Field to use.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $metadata
   *   Cache metadata.
   *
   * @return array
   *   Media field data.
   */
  public function resolve(MediaInterface $media, string $field, RefinableCacheableDependencyInterface $metadata): array {
    $metadata->addCacheableDependency($media);

    return $media->hasField($field) ? $media->get($field)->getValue() : [];
  }

}
