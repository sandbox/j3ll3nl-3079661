<?php

namespace Drupal\frmwrk_decoupled_media\Plugin\GraphQL\DataProducer;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\file\Entity\File;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;

/**
 * Class MediaFieldResolver.
 *
 * @DataProducer(
 *   id = "media_field_resolver",
 *   name = @Translation("media field resolver"),
 *   description = @Translation("Resolve a specific media field "),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Media field value")
 *   ),
 *   consumes = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("File"),
 *       required = TRUE
 *     ),
 *    "field" = @ContextDefinition("string",
 *       label = @Translation("Field"),
 *       required = TRUE
 *     ),
 *   }
 * )
 *
 * @package Drupal\frmwrk_decoupled_media\Plugin\GraphQL\DataProducer
 */
class MediaFieldResolver extends DataProducerPluginBase {

  /**
   * Resolves a media field.
   *
   * @param \Drupal\file\Entity\File $entity
   *   Media or media field.
   * @param string $field
   *   Media field to resolve.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $metadata
   *   Cache metadata.
   *
   * @return string|null
   *   Field value or null.
   */
  public function resolve(File $entity, string $field, RefinableCacheableDependencyInterface $metadata) {
    $metadata->addCacheableDependency($entity);

    return $entity->hasField($field) ? $entity->get($field)->value : NULL;
  }

}
