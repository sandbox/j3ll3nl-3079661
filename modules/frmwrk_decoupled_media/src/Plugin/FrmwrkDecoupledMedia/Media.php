<?php

namespace Drupal\frmwrk_decoupled_media\Plugin\FrmwrkDecoupledMedia;

use Drupal\frmwrk_decoupled_media\FrmwrkDecoupledMediaPluginBase;

/**
 * Plugin implementation of the frmwrk_decoupled_media.
 *
 * @FrmwrkDecoupledMedia(
 *   id = "media",
 *   description = @Translation("Adds media types."),
 * )
 *
 * @package Drupal\frmwrk_decoupled_media\Plugin\FrmwrkDecoupledMedia
 */
class Media extends FrmwrkDecoupledMediaPluginBase {

  /**
   * {@inheritdoc}
   */
  public function addMedia(): array {
    return [
      // Media.
      'Image' => [
        'id' => 'entity_id',
        'filename' => 'media_field_resolver',
        'alt' => 'image_alt_resolver',
      ],
      'File' => [
        'id' => 'entity_id',
        'filename' => 'media_field_resolver',
        'uri' => 'media_uri_resolver',
        'filemime' => 'media_field_resolver',
        'filesize' => 'media_field_resolver',
      ],
      'Video' => [
        'id' => 'entity_id',
        'filename' => 'media_field_resolver',
        'uri' => 'media_uri_resolver',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getMediaTypeResolvers(): array {
    return [
      // Drupal media => graphql interface implementation.
      'media' => 'Media',
      'image' => 'Image',
      'file' => 'File',
    ];
  }

}
