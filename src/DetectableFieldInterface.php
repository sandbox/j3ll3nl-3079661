<?php

namespace Drupal\frmwrk_decoupled;

/**
 * Interface DetectableFieldInterface.
 *
 * @package Drupal\frmwrk_decoupled
 */
interface DetectableFieldInterface {

  /**
   * Return the default field name.
   *
   * Used when no field name was found.
   *
   * @return string
   *   Default field name.
   */
  public function getDefaultFieldName(): string;

  /**
   * Return the target type to compare against.
   *
   * @return string
   *   Target type.
   */
  public function getTargetType(): string;

  /**
   * Return the detected fields for a specific bundle.
   *
   * @param string $bundle
   *   Bundle to check for fields.
   *
   * @return array
   *   Field names.
   */
  public function getFieldNamesByBundle(string $bundle): array;

}
