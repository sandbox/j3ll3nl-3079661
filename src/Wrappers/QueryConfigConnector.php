<?php

namespace Drupal\frmwrk_decoupled\Wrappers;

/**
 * Class QueryConfigConnector.
 */
class QueryConfigConnector {

  /**
   * Configuration options.
   *
   * @var array
   */
  protected $configurations;

  /**
   * QueryConnection constructor.
   *
   * @param array $configurations
   *   Configuration options.
   */
  public function __construct(array $configurations) {
    $this->configurations = $configurations;
  }

  /**
   * Getter for configuration options.
   *
   * @param string $name
   *   Configuration name.
   *
   * @return string
   *   Configuration option.
   */
  public function getConfig(string $name) {
    return $this->configurations[$name];
  }

}
