<?php

namespace Drupal\frmwrk_decoupled\Wrappers;

/**
 * Interface ConnectionInterface.
 *
 * @package Drupal\frmwrk_decoupled\Wrappers
 */
interface ConnectionInterface {

  /**
   * Return total results.
   *
   * @return int
   *   Total results.
   *
   * @throws \Drupal\search_api\SearchApiException
   *   Thrown if an error occurred during the search.
   */
  public function total(): int;

  /**
   * Get result items.
   *
   * @return array|\GraphQL\Deferred
   *   Result.
   */
  public function items();

}
