<?php

namespace Drupal\frmwrk_decoupled;

/**
 * Interface for frmwrk_decoupled_nodes plugins.
 */
interface FrmwrkDecoupledNodesInterface {

  /**
   * Assign resolvers to node fields.
   *
   * @return array
   *   Resolver array.
   */
  public function addNodes();

  /**
   * Return the file location for the base .graphqls file.
   *
   * The path must not contain the file extension.
   *
   * @return string
   *   GraphQL base location.
   */
  public function getGraphqlBaseFile(): string;

}
