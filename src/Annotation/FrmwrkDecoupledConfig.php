<?php

namespace Drupal\frmwrk_decoupled\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines frmwrk_decoupled_config annotation object.
 *
 * @Annotation
 */
class FrmwrkDecoupledConfig extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The part of the config that the module needs.
   *
   * @var string
   */
  public $configChunk;

}
