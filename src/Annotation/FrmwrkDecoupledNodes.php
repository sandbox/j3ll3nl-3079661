<?php

namespace Drupal\frmwrk_decoupled\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines frmwrk_decoupled_nodes annotation object.
 *
 * @Annotation
 */
class FrmwrkDecoupledNodes extends Plugin {

  /**
   * Plugin id.
   *
   * @var string
   */
  public $id;

  /**
   * The node type.
   *
   * @var string
   */
  public $nodeType;

  /**
   * GraphQL implementation for the node.
   *
   * @var string
   */
  public $resolver;

}
