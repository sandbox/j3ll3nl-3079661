<?php

namespace Drupal\frmwrk_decoupled;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for frmwrk_decoupled_nodes plugins.
 */
abstract class FrmwrkDecoupledNodesPluginBase extends PluginBase implements FrmwrkDecoupledNodesInterface {

  /**
   * {@inheritdoc}
   */
  public function addNodes() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getGraphqlBaseFile(): string {
    return $this->loadFileDefinition('base');
  }

  /**
   * Returns schema location.
   *
   * @param string $type
   *   The type of file base|extension.
   *
   * @return string
   *   File path.
   */
  protected function loadFileDefinition(string $type) {
    /** @var \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler */
    $moduleHandler = \Drupal::service('module_handler');

    $id = $this->getPluginId();
    $definition = $this->getPluginDefinition();
    $module = $moduleHandler->getModule($definition['provider']);
    return "{$module->getPath()}/graphql/{$id}.{$type}.graphqls";
  }

}
