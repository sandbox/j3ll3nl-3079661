<?php

namespace Drupal\frmwrk_decoupled\Plugin\GraphQL\Schema;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\frmwrk_decoupled\FrmwrkDecoupledNodesPluginManager;
use Drupal\frmwrk_decoupled\services\NodeDetectorService;
use Drupal\frmwrk_decoupled\Wrappers\ConnectionInterface;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistry;
use Drupal\graphql\Plugin\GraphQL\Schema\SdlSchemaPluginBase;
use Drupal\graphql\Plugin\SchemaExtensionPluginManager;
use Drupal\node\NodeInterface;
use GraphQL\Error\Error;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class FrmwrkSchema.
 *
 * @Schema(
 *   id = "frmwrk",
 *   name = "Frmwrk schema"
 * )
 */
class FrmwrkSchema extends SdlSchemaPluginBase {

  use StringTranslationTrait;

  /**
   * Node types with graphQL resolvers.
   *
   * @var array
   */
  private $nodeTypes = [];

  /**
   * Collection of graphQL files that need to be merged.
   *
   * @var array
   */
  private $graphQLFiles = [];

  /**
   * Fields for different nodes.
   *
   * @var array
   */
  private $nodeFields = [];

  /**
   * @var \Drupal\frmwrk_decoupled\FrmwrkDecoupledNodesPluginManager
   */
  private $nodesPluginManager;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('cache.graphql.ast'),
      $container->get('module_handler'),
      $container->get('plugin.manager.graphql.schema_extension'),
      $container->getParameter('graphql.config'),
      $container->get('frmwrk_decoupled.services.node_detector_service'),
      $container->get('plugin.manager.frmwrk_decoupled_nodes')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function __construct(
    $configuration,
    $pluginId,
    $pluginDefinition,
    CacheBackendInterface $astCache,
    ModuleHandlerInterface $moduleHandler,
    SchemaExtensionPluginManager $extensionManager,
    $config,
    NodeDetectorService $nodeDetectorService,
    FrmwrkDecoupledNodesPluginManager $nodesPluginManager
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition, $astCache, $moduleHandler, $extensionManager, $config);

    $this->graphQLFiles = $nodeDetectorService->getNodeDefinitions();
    $this->nodeTypes = $nodeDetectorService->getNodeTypes();
    $this->nodesPluginManager = $nodesPluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getResolverRegistry() {
    $builder = new ResolverBuilder();
    $registry = new ResolverRegistry();

    foreach ($this->nodesPluginManager->getDefinitions() as $pluginDefinition) {
      /** @var \Drupal\frmwrk_decoupled\FrmwrkDecoupledNodesInterface $plugin */
      $plugin = $this->nodesPluginManager->createInstance($pluginDefinition['id']);

      $this->nodeFields = array_merge($this->nodeFields, $plugin->addNodes());
    }

    $this->addQueryFields($registry, $builder);

    foreach ($this->nodeTypes as $_ => $def) {
      $this->addDefaultNodeFields($def, $registry, $builder);
    }

    // Re-usable connection type fields.
    $this->addConnectionFields('NodeConnection', $registry, $builder);

    $this->addInterfaces($registry, $builder);

    $this->addConfig($registry, $builder);

    $this->addMenu($registry, $builder);

    return $registry;
  }

  /**
   * Add node field resolvers.
   *
   * @param string $parent
   *   Parent type.
   * @param array $fields
   *   Fields to add.
   * @param \Drupal\graphql\GraphQL\ResolverRegistry $registry
   *   Resolverregistry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   Resolverbuilder.
   */
  private function addNodeFields(string $parent, array $fields, ResolverRegistry $registry, ResolverBuilder $builder) {
    foreach ($fields as $field => $resolver) {
      if (is_array($resolver)) {
        $this->addNodeFields($field, $resolver, $registry, $builder);

        $resolver = 'node_array_field_resolver';
      }

      $registry->addFieldResolver($parent, $field,
        $builder->produce($resolver)
          ->map('entity', $builder->fromParent())
          ->map('field', $builder->fromValue($field))
      );
    }
  }

  /**
   * Add default node fields.
   *
   * @param string $type
   *   Node type.
   * @param \Drupal\graphql\GraphQL\ResolverRegistry $registry
   *   Resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   Resolver builder.
   */
  protected function addDefaultNodeFields(string $type, ResolverRegistry $registry, ResolverBuilder $builder) {
    $registry->addFieldResolver($type, 'id',
      $builder->produce('entity_id')
        ->map('entity', $builder->fromParent())
    );

    $registry->addFieldResolver($type, 'title',
      $builder->compose(
        $builder->produce('entity_label')
          ->map('entity', $builder->fromParent())
      )
    );

    $registry->addFieldResolver($type, 'author',
      $builder->compose(
        $builder->produce('entity_owner')
          ->map('entity', $builder->fromParent()),
        $builder->produce('entity_label')
          ->map('entity', $builder->fromParent())
      )
    );

    $registry->addFieldResolver($type, 'related',
      $builder->produce('related_content_resolver')
        ->map('entity', $builder->fromParent())
        ->map('field', $builder->fromValue('field_related'))
    );

    $registry->addFieldResolver($type, 'url',
      $builder->produce('url_path')
        ->map('url', $builder->produce('entity_url')
          ->map('entity', $builder->fromParent())
        )
    );

    if (isset($this->nodeFields[$type])) {
      $this->addNodeFields($type, $this->nodeFields[$type], $registry, $builder);
    }
  }

  /**
   * Add query fields.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistry $registry
   *   Resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   Resolver builder.
   */
  protected function addQueryFields(ResolverRegistry $registry, ResolverBuilder $builder) {
    $registry->addFieldResolver('Query', 'node',
      $builder->produce('entity_load')
        ->map('type', $builder->fromValue('node'))
        ->map('id', $builder->fromArgument('id'))
    );

    $registry->addFieldResolver('Query', 'nodes',
      $builder->produce('query_nodes')
        ->map('offset', $builder->fromArgument('offset'))
        ->map('bundles', $builder->fromArgument('bundles'))
        ->map('limit', $builder->fromArgument('limit'))
        ->map('sort', $builder->fromArgument('sort'))
        ->map('column', $builder->fromArgument('column'))
    );
  }

  /**
   * Add connection fields.
   *
   * @param string $type
   *   Field type.
   * @param \Drupal\graphql\GraphQL\ResolverRegistry $registry
   *   Resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   Resolver builder.
   */
  protected function addConnectionFields(string $type, ResolverRegistry $registry, ResolverBuilder $builder) {
    $registry->addFieldResolver($type, 'total',
      $builder->callback(function (ConnectionInterface $connection) {
        return $connection->total();
      })
    );

    $registry->addFieldResolver($type, 'items',
      $builder->callback(function (ConnectionInterface $connection) {
        return $connection->items();
      })
    );
  }

  /**
   * Add interfaces.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistry $registry
   *   Resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   Resolver builder.
   */
  private function addInterfaces(ResolverRegistry $registry, ResolverBuilder $builder): void {
    // Tell GraphQL how to resolve types of a common interface.
    $registry->addTypeResolver('NodeInterface', function ($value) {
      if ($value instanceof NodeInterface) {
        return $this->nodeTypes[$value->bundle()] ?? "Node";
      }

      throw new Error($this->t('Could not resolve content type.'));
    });

    $registry->addFieldResolver('Query', 'route', $builder->compose(
      $builder->produce('route_load_extended')
        ->map('path', $builder->fromArgument('path')),
      $builder->produce('route_entity')
        ->map('url', $builder->fromParent())
    ));
  }

  /**
   * Add config.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistry $registry
   *   Resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   Resolver builder.
   */
  private function addConfig(ResolverRegistry $registry, ResolverBuilder $builder): void {
    $registry->addFieldResolver('Query', 'config',
      $builder->produce('query_config')
    );
  }

  /**
   * Add menu.
   *
   * @param \Drupal\graphql\GraphQL\ResolverRegistry $registry
   *   Resolver registry.
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   *   Resolver builder.
   */
  private function addMenu(ResolverRegistry $registry, ResolverBuilder $builder) {
    // Menu query.
    $registry->addFieldResolver('Query', 'menu',
      $builder->produce('entity_load')
        ->map('type', $builder->fromValue('menu'))
        ->map('id', $builder->fromArgument('name'))
        ->map('access', $builder->fromValue(FALSE))
    );

    // Menu name.
    $registry->addFieldResolver('Menu', 'name',
      $builder->produce('property_path')
        ->map('type', $builder->fromValue('entity:menu'))
        ->map('value', $builder->fromParent())
        ->map('path', $builder->fromValue('label'))
    );

    // Menu items.
    $registry->addFieldResolver('Menu', 'items',
      $builder->produce('menu_links')
        ->map('menu', $builder->fromParent())
    );

    // Menu title.
    $registry->addFieldResolver('MenuItem', 'title',
      $builder->produce('menu_link_label')
        ->map('link', $builder->produce('menu_tree_link')
          ->map('element', $builder->fromParent())
        )
    );

    // Menu children.
    $registry->addFieldResolver('MenuItem', 'children',
      $builder->produce('menu_tree_subtree')
        ->map('element', $builder->fromParent())
    );

    // Menu url.
    $registry->addFieldResolver('MenuItem', 'url',
      $builder->produce('menu_link_url')
        ->map('link', $builder->produce('menu_tree_link')
          ->map('element', $builder->fromParent()
          ))
    );

    $registry->addFieldResolver('Url', 'path',
      $builder->produce('url_path')
        ->map('url', $builder->fromParent())
    );
  }

  /**
   * {@inheritdoc}
   *
   * Overwrite the default behaviour of this function to merge our discovered
   * graphql files in.
   */
  protected function getSchemaDefinition() {
    $file = parent::getSchemaDefinition();

    foreach ($this->graphQLFiles as $gqlFile) {
      if (!file_exists($gqlFile)) {
        throw new InvalidPluginDefinitionException(sprintf("Missing schema definition file at %s.", $gqlFile));
      }

      $file .= "\n" . file_get_contents($gqlFile);
    }

    return $file;
  }

}
