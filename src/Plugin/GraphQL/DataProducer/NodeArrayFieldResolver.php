<?php

namespace Drupal\frmwrk_decoupled\Plugin\GraphQL\DataProducer;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\node\NodeInterface;

/**
 * Class NodeArrayFieldResolver.
 *
 * @DataProducer(
 *   id = "node_array_field_resolver",
 *   name = @Translation("node array field resolver"),
 *   description = @Translation("Resolve a specific node array type field "),
 *   produces = @ContextDefinition("list",
 *     label = @Translation("Node array field value(s)")
 *   ),
 *   consumes = {
 *     "entity" = @ContextDefinition("any",
 *       label = @Translation("Node|Object"),
 *       required = TRUE
 *     ),
 *    "field" = @ContextDefinition("string",
 *       label = @Translation("Field"),
 *       required = TRUE
 *     ),
 *   }
 * )
 *
 * @package Drupal\frmwrk_decoupled\Plugin\GraphQL\DataProducer
 */
class NodeArrayFieldResolver extends DataProducerPluginBase {

  /**
   * Resolves a node field.
   *
   * @param \Drupal\node\NodeInterface $node
   *   Node or Node field.
   * @param string $field
   *   Node field to resolve.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $metadata
   *   Cache metadata.
   *
   * @return array
   *   Field value or empty array.
   */
  public function resolve(NodeInterface $node, string $field, RefinableCacheableDependencyInterface $metadata): array {
    $metadata->addCacheableDependency($node);

    if (!$node->hasField($field)) {
      return [];
    }

    if (is_array($node->get($field)->getValue())) {
      $fieldValue = $node->get($field)->getValue();

      if (isset(reset($fieldValue)['target_id'])) {
        return $node->get($field)->referencedEntities();
      }

      return reset($fieldValue);
    }

    return $node->get($field)->value;
  }

}
