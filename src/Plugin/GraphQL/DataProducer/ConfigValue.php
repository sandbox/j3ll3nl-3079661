<?php

namespace Drupal\frmwrk_decoupled\Plugin\GraphQL\DataProducer;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;

/**
 * Class ConfigValue.
 *
 * @DataProducer(
 *   id = "config_value",
 *   name = @Translation("Get value from config"),
 *   description = @Translation("Loads config from array in configuration"),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Variables")
 *   ),
 *   consumes = {
 *     "config" = @ContextDefinition("any",
 *       label = @Translation("Configuration"),
 *       required = TRUE
 *     ),
 *     "name" = @ContextDefinition("string",
 *       label = @Translation("Configuration"),
 *       required = TRUE
 *     ),
 *   }
 * )
 */
class ConfigValue extends DataProducerPluginBase {

  /**
   * Config value resolver.
   *
   * @param array $config
   *   Config array.
   * @param string $name
   *   Config name.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $metadata
   *   Cache metadata.
   *
   * @return string
   *   Config value.
   */
  public function resolve(array $config, string $name, RefinableCacheableDependencyInterface $metadata) {
    return isset($config[$name]) ? $config[$name] : '';
  }

}
