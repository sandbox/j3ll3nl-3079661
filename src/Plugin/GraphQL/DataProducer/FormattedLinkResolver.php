<?php

namespace Drupal\frmwrk_decoupled\Plugin\GraphQL\DataProducer;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Url;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;

/**
 * Class FormattedLinkResolver.
 *
 * @DataProducer(
 *   id = "formatted_link_resolver",
 *   name = @Translation("Drupal url"),
 *   description = @Translation("Return a link"),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("link")
 *   ),
 *   consumes = {
 *     "link_array" = @ContextDefinition("any",
 *       label = @Translation("Media|Object"),
 *       required = TRUE
 *     ),
 *   }
 * )
 *
 * @package Drupal\frmwrk_decoupled_media\Plugin\GraphQL\DataProducer
 */
class FormattedLinkResolver extends DataProducerPluginBase {

  /**
   * Return link data array.
   *
   * @param mixed $link_array
   *   Link.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $metadata
   *   Caching metadata.
   *
   * @return string
   *   Link.
   */
  public function resolve($link_array, RefinableCacheableDependencyInterface $metadata) {
    if (empty($link_array)) {
      return NULL;
    }
    $url = Url::fromUri($link_array['uri']);

    $metadata->addCacheableDependency($url);

    return $url;
  }

}
