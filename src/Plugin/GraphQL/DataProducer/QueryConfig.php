<?php

namespace Drupal\frmwrk_decoupled\Plugin\GraphQL\DataProducer;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\frmwrk_decoupled\FrmwrkDecoupledConfigPluginManager;
use Drupal\frmwrk_decoupled\Wrappers\QueryConfigConnector;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class QueryConfig.
 *
 * @DataProducer(
 *   id = "query_config",
 *   name = @Translation("Load config"),
 *   description = @Translation("Loads config from Drupal"),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Variables")
 *   ),
 *   consumes = {
 *     "language" = @ContextDefinition("string",
 *       label = @Translation("Language"),
 *       required = FALSE
 *     ),
 *   }
 * )
 */
class QueryConfig extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $configFactory;

  /**
   * Config plugin manager.
   *
   * @var \Drupal\frmwrk_decoupled\FrmwrkDecoupledConfigPluginManager
   */
  protected $pluginManager;

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): QueryConfig {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('plugin.manager.frmwrk_decoupled_config')
    );
  }

  /**
   * Basic pages constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $pluginId
   *   The plugin id.
   * @param mixed $pluginDefinition
   *   The plugin definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   * @param \Drupal\frmwrk_decoupled\FrmwrkDecoupledConfigPluginManager $pluginManager
   *   Config plugin manager.
   *
   * @codeCoverageIgnore
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    ConfigFactoryInterface $configFactory,
    FrmwrkDecoupledConfigPluginManager $pluginManager
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->configFactory = $configFactory;
    $this->pluginManager = $pluginManager;
  }

  /**
   * Get config.
   *
   * @param string $configName
   *   Configuration name.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   Config value.
   */
  public function getConfig(string $configName) {
    return $this->configFactory->get($configName);
  }

  /**
   * Config query resolver.
   *
   * @param $language
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $metadata
   *   Cache metadata.
   *
   * @return \Drupal\frmwrk_decoupled\Wrappers\QueryConfigConnector
   *   Query connection.
   */
  public function resolve($language, RefinableCacheableDependencyInterface $metadata) {
    $configurations = [];

    foreach ($this->pluginManager->getDefinitions() as $plugin) {
      $configName = $plugin['configChunk'];

      if (!$configuration = $this->getConfig($configName)) {
        continue;
      }
      $metadata->addCacheTags($configuration->getCacheTags());
      $metadata->addCacheContexts($configuration->getCacheContexts());
      $configurations[$configName] = $configuration->get();
    }

    return new QueryConfigConnector($configurations);
  }

}
