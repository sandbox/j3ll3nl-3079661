<?php

namespace Drupal\frmwrk_decoupled\Plugin\GraphQL\DataProducer;

/**
 * Class ParagraphParagraphFieldResolver.
 *
 * @DataProducer(
 *   id = "related_content_resolver",
 *   name = @Translation("Node related content"),
 *   description = @Translation("Resolve a node's related content"),
 *   produces = @ContextDefinition("list",
 *     label = @Translation("RElated content")
 *   ),
 *   consumes = {
 *     "entity" = @ContextDefinition("entity",
 *       label = @Translation("Paragraph"),
 *       required = TRUE
 *     ),
 *    "field" = @ContextDefinition("string",
 *       label = @Translation("Field"),
 *       required = TRUE
 *     ),
 *   }
 * )
 *
 * @package Drupal\frmwrk_decoupled_paragraphs\Plugin\GraphQL\DataProducer
 */
class RelatedContentResolver extends EntityReflectionBaseResolver {

  /**
   * {@inheritdoc}
   */
  protected function reflectEntity(array $entityReference) {
    $entity = parent::reflectEntity($entityReference);

    return $entity->isPublished() ? $entity : NULL;
  }

  /**
   * {@inheritDoc}
   */
  protected function getEntity(): string {
    return 'node';
  }

}
