<?php

namespace Drupal\frmwrk_decoupled\Plugin\GraphQL\DataProducer;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\frmwrk_decoupled\Wrappers\QueryConfigConnector;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;

/**
 * Class ConfigObject.
 *
 * @DataProducer(
 *   id = "config_object",
 *   name = @Translation("Load config from config connector"),
 *   description = @Translation("Loads config from connector"),
 *   produces = @ContextDefinition("list",
 *     label = @Translation("Variables")
 *   ),
 *   consumes = {
 *     "configs" = @ContextDefinition("any",
 *       label = @Translation("Configuration"),
 *       required = TRUE
 *     ),
 *     "name" = @ContextDefinition("string",
 *       label = @Translation("Configuration"),
 *       required = TRUE
 *     ),
 *   }
 * )
 */
class ConfigObject extends DataProducerPluginBase {

  /**
   * Config object resolver.
   *
   * @param \Drupal\frmwrk_decoupled\Wrappers\QueryConfigConnector $configs
   *   Query connection.
   * @param string $name
   *   Config name.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $metadata
   *   Cache metadata.
   *
   * @return mixed
   *   Config value.
   */
  public function resolve(QueryConfigConnector $configs, string $name, RefinableCacheableDependencyInterface $metadata) {

    $metadata->addCacheTags([$name]);

    return $configs->getConfig($name);
  }

}
