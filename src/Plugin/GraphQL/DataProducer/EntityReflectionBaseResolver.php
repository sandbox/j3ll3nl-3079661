<?php

namespace Drupal\frmwrk_decoupled\Plugin\GraphQL\DataProducer;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EntityReflectionBaseResolver.
 *
 * This class is useless on it's own and must be extended by a DataProducer.
 * Every class extending this abstract class must define the \@DataProducer
 * annotation to be picked up as valid DataProducer.
 *
 * @see \Drupal\frmwrk_decoupled_paragraphs\Plugin\GraphQL\DataProducer\ParagraphParagraphFieldResolver
 *
 * @package Drupal\frmwrk_decoupled\Plugin\GraphQL\DataProducer
 */
abstract class EntityReflectionBaseResolver extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Entity storage object.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ): EntityReflectionBaseResolver {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityFieldManagerInterface $entityFieldManager,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityFieldManager = $entityFieldManager;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Get the target entity type.
   *
   * Function must be overwritten by concrete classes.
   *
   * @return string
   *   EntityStorage type to be loaded.
   */
  abstract protected function getEntity(): string;

  /**
   * Resolve an entity on a paragraph returning the entity.
   *
   * This resolver can be extended to resolve for example a paragraph or a media
   * item on a paragraph.
   *
   * @param \Drupal\paragraphs\ParagraphInterface|\Drupal\node\NodeInterface $entity
   *   Paragraph containing the field.
   * @param string $field
   *   Field to reflect.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $metadata
   *   Cache metadata.
   *
   * @return \Drupal\Core\Entity\EntityInterface|\Drupal\Core\Entity\EntityInterface[]
   *   Entity or a collection of entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the entity type doesn't exist.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the storage handler couldn't be loaded.
   */
  final protected function resolve(
    EntityInterface $entity,
    string $field,
    RefinableCacheableDependencyInterface $metadata
  ) {
    $metadata->addCacheableDependency($entity);

    if (!($entity->hasField($field) && $entity->get($field) instanceof EntityReferenceFieldItemList)) {
      return [];
    }

    $cardinality = $entity->get($field)->getFieldDefinition()->get('fieldStorage')->getCardinality();

    $entityIds = $entity->get($field)->getValue();
    // Prevent returning a collection of arrays on one entity.
    if ($cardinality == 1) {
      return $this->reflectEntity($entityIds[0]);
    }
    else {
      $output = [];
      foreach ($entityIds as $entityId) {
        $output[] = $this->reflectEntity($entityId);
      }

      return $output;
    }
  }

  /**
   * Reflect an entity.
   *
   * @param array $entityReference
   *   Entity reference.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the entity type doesn't exist.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the storage handler couldn't be loaded.
   */
  protected function reflectEntity(array $entityReference) {
    $entity = $this->getEntity();
    $entityStorage = $this->entityTypeManager->getStorage($entity);

    return $entityStorage->load($entityReference['target_id']);
  }

}
