<?php

namespace Drupal\frmwrk_decoupled\Plugin\GraphQL\DataProducer;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\node\NodeInterface;

/**
 * Class NodeFieldResolver.
 *
 * @DataProducer(
 *   id = "node_field_resolver",
 *   name = @Translation("node field resolver"),
 *   description = @Translation("Resolve a specific node field "),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Node field value")
 *   ),
 *   consumes = {
 *     "entity" = @ContextDefinition("any",
 *       label = @Translation("Node|Object"),
 *       required = TRUE
 *     ),
 *    "field" = @ContextDefinition("string",
 *       label = @Translation("Field"),
 *       required = TRUE
 *     ),
 *   }
 * )
 *
 * @package Drupal\frmwrk_decoupled\Plugin\GraphQL\DataProducer
 */
class NodeFieldResolver extends DataProducerPluginBase {

  /**
   * Resolves a node field.
   *
   * @param \Drupal\node\NodeInterface $node
   *   Node or Node field.
   * @param string $field
   *   Node field to resolve.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $metadata
   *   Cache metadata.
   *
   * @return string|null
   *   Field value or null.
   */
  public function resolve(NodeInterface $node, string $field, RefinableCacheableDependencyInterface $metadata) {
    $metadata->addCacheableDependency($node);

    if (!$node->hasField($field)) {
      return NULL;
    }

    return $node->get($field)->value;
  }

}
