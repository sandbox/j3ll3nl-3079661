<?php

namespace Drupal\frmwrk_decoupled\Plugin\GraphQL\DataProducer;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\frmwrk_decoupled\DetectableFieldInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\frmwrk_decoupled\Wrappers\QueryConnection;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\taxonomy\Entity\Term;
use GraphQL\Error\UserError;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class QueryNodes.
 *
 * @DataProducer(
 *   id = "query_nodes",
 *   name = @Translation("Load nodes"),
 *   description = @Translation("Loads a list of nodes."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Basic pages connection.")
 *   ),
 *   consumes = {
 *     "offset" = @ContextDefinition("integer",
 *       label = @Translation("Offset."),
 *       required = FALSE
 *     ),
 *     "limit" = @ContextDefinition("integer",
 *       label = @Translation("Limit."),
 *       required = FALSE
 *     ),
 *     "bundles" = @ContextDefinition("string",
 *       label = @Translation("Bundles comma seperated."),
 *       required = FALSE
 *     ),
 *     "column" = @ContextDefinition("string",
 *       label = @Translation("Sorting colun."),
 *       required = FALSE
 *     ),
 *     "sort" = @ContextDefinition("string",
 *       label = @Translation("Sorting direction (ASC/DESC)."),
 *       required = FALSE
 *     ),
 *     "excluded" = @ContextDefinition("integer",
 *       label = @Translation("Parent id to be excluded from response."),
 *       required = FALSE
 *     ),
 *     "paragraph" = @ContextDefinition("entity",
 *       label = @Translation("List paragraph."),
 *       required = FALSE
 *     )
 *   }
 * )
 */
class QueryNodes extends DataProducerPluginBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  const MAX_LIMIT = 100;
  const SORT_ASC = "ASC";
  const SORT_DESC = "DESC";

  /**
   * Entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Entity Field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Taxonomy field detector service.
   *
   * @var \Drupal\frmwrk_decoupled\DetectableFieldInterface
   */
  protected $taxonomyFieldDetectorService;

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('frmwrk_decoupled.services.taxonomy_field_detector_service')
    );
  }

  /**
   * Basic pages constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $pluginId
   *   The plugin id.
   * @param mixed $pluginDefinition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   Entity field manager.
   * @param \Drupal\frmwrk_decoupled\DetectableFieldInterface $taxonomyFieldDetectorService
   *   Taxonomy field detector service.
   *
   * @codeCoverageIgnore
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    EntityTypeManagerInterface $entityTypeManager,
    EntityFieldManagerInterface $entityFieldManager,
    DetectableFieldInterface $taxonomyFieldDetectorService
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->entityTypeManager = $entityTypeManager;
    $this->entityFieldManager = $entityFieldManager;
    $this->taxonomyFieldDetectorService = $taxonomyFieldDetectorService;
  }

  /**
   * Node query resolver.
   *
   * @param int $offset
   *   Offset.
   * @param int $limit
   *   Limit.
   * @param string $bundles
   *   Node bundles to filter on.
   * @param string $column
   *   Sorting column.
   * @param string $sort
   *   Sorting direction (asc/desc) defaults to asc.
   * @param int $parent
   *   Parent node id, to be excluded from this result.
   * @param \Drupal\paragraphs\ParagraphInterface|null $paragraph
   *   List paragraph for configuration.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $metadata
   *   Cache metadata.
   *
   * @return \Drupal\frmwrk_decoupled\Wrappers\QueryConnection
   *   Query connection.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Exception
   */
  public function resolve($offset, $limit, $bundles, $column, $sort, $parent, $paragraph, RefinableCacheableDependencyInterface $metadata): QueryConnection {
    // Check for a paragraph that can be used to generate a query connection.
    if ($paragraph && $paragraph instanceof ParagraphInterface && $paragraph->bundle() === "content_list") {
      // Load the config paragraph.
      $filterParagraph = Paragraph::load($paragraph->get("field_filter_options")->target_id);
      // Check for the correct type.
      if ($filterParagraph->bundle() !== "list_filter_options") {
        throw new \Exception($this->t("Expected paragraph of type list_filter_options, found @paragraphType", [
          "@paragraphType" => $filterParagraph->bundle(),
        ]));
      }

      // Check if default arguments are passed eg. direct query or list
      // paragraph with overwritten arguments, in this case use parsed instead.
      // If the parsed argument is null or none is parsed try to get a value
      // from the list paragraph, if that value is null opr not set fallback to
      // a default value.
      $offset = $offset ?? $filterParagraph->get('field_filter_offset')->value ?? 0;
      $limit = $limit ?? $filterParagraph->get('field_filter_limit')->value ?? 10;
      $bundles = $bundles ?? $paragraph->get("field_content_type")->value;
      $column = $column ?? $filterParagraph->get('field_filter_column')->value ?? "changed";
      $sort = $sort ?? $filterParagraph->get('field_sorting_direction')->value ?? self::SORT_ASC;
      $parent = $parent ?? $paragraph->getParentEntity()->id();

      $referencedEntities = $paragraph->get('field_category')->referencedEntities() ?? NULL;
      if ($referencedEntities && $referencedEntities[0] instanceof Term) {
        /** @var \Drupal\taxonomy\Entity\Term $term */
        $term = $referencedEntities[0];
        $taxonomyTerm['term'] = $term;

        $fields = $this->taxonomyFieldDetectorService->getFieldNamesByBundle($bundles);
        foreach ($fields as $field) {
          $targetTypes = $this->entityFieldManager->getFieldDefinitions('node', $bundles)[$field]->getSetting('handler_settings')['target_bundles'];
          if (in_array($term->bundle(), array_keys($targetTypes))) {
            $taxonomyTerm['fields'][] = $field;
          }
        }
      }
    }

    return $this->resolveFromValues($offset, $limit, $bundles, $taxonomyTerm ?? NULL, $column, $sort, $parent, $metadata);
  }

  /**
   * Resolves a node query based on params.
   *
   * @param int $offset
   *   Offset.
   * @param int $limit
   *   Limit.
   * @param string $bundles
   *   Node bundles to filter on.
   * @param mixed $taxonomyTerm
   *   Optional taxonomy term to narrow the search down.
   * @param string $column
   *   Sorting column.
   * @param string $sort
   *   Sorting direction (asc/desc) defaults to asc.
   * @param int $excluded
   *   Parent node id, to be excluded from this result.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $metadata
   *   Cache metadata.
   *
   * @return \Drupal\frmwrk_decoupled\Wrappers\QueryConnection
   *   Query connection.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function resolveFromValues(int $offset, int $limit, string $bundles, $taxonomyTerm, string $column, string $sort, $excluded, RefinableCacheableDependencyInterface $metadata): QueryConnection {
    if (!$limit > static::MAX_LIMIT) {
      throw new UserError(sprintf('Exceeded maximum query limit: %s.', static::MAX_LIMIT));
    }

    $storage = $this->entityTypeManager->getStorage('node');
    $type = $storage->getEntityType();
    $query = $storage->getQuery()
      ->currentRevision()
      ->accessCheck();

    $query->condition($type->getKey('bundle'), explode(',', $bundles), 'IN');
    $query->condition('status', 1);
    $query->range($offset, $limit);
    if ($column) {
      if (!$sort || !in_array($sort, [self::SORT_ASC, self::SORT_DESC])) {
        $sort = self::SORT_ASC;
      }
      $query->sort($column, $sort);
    }
    if ($excluded) {
      $query->condition('nid', $excluded, '!=');
    }
    if ($taxonomyTerm) {
      $term = $taxonomyTerm['term'];
      $group = $query->orConditionGroup();
      foreach ($taxonomyTerm['fields'] as $field) {
        $group->condition($field, $term->id());
      }

      $query->condition($group);
    }

    $metadata->addCacheTags($type->getListCacheTags());
    $metadata->addCacheContexts($type->getListCacheContexts());

    return new QueryConnection($query);
  }

}
