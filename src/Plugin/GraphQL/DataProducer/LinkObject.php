<?php

namespace Drupal\frmwrk_decoupled\Plugin\GraphQL\DataProducer;

use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;

/**
 * Class LinkObject.
 *
 * @DataProducer(
 *   id = "link_object",
 *   name = @Translation("Link object"),
 *   description = @Translation("Return a link array"),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Media file object")
 *   ),
 *   consumes = {
 *     "link" = @ContextDefinition("any",
 *       label = @Translation("Media|Object"),
 *       required = TRUE
 *     ),
 *     "field" = @ContextDefinition("any",
 *       label = @Translation("String field"),
 *       required = TRUE
 *     ),
 *   }
 * )
 *
 * @package Drupal\frmwrk_decoupled_media\Plugin\GraphQL\DataProducer
 */
class LinkObject extends DataProducerPluginBase {

  /**
   * Return link data array.
   *
   * @param mixed $link
   *   Link.
   * @param mixed $field
   *   Field to get data from.
   * @param \Drupal\Core\Cache\RefinableCacheableDependencyInterface $metadata
   *   Caching metadata.
   *
   * @return array
   *   Link array.
   */
  public function resolve($link, $field, RefinableCacheableDependencyInterface $metadata) {
    $metadata->addCacheableDependency($link);
    $props = ['title' => '', 'uri' => ''];

    $rawFieldValue = $link->get($field)->getValue();

    if (empty($rawFieldValue)) {
      return [];
    }

    $return = [];

    if (count($rawFieldValue) > 1) {
      foreach ($rawFieldValue as $fieldValue) {
        $return[] = array_intersect_key($fieldValue, $props);
      }

      return $return;
    }
    else {
      return array_intersect_key(reset($rawFieldValue), $props);
    }

  }

}
