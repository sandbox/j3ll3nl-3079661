<?php

namespace Drupal\frmwrk_decoupled\Plugin\FrmwrkDecoupledNodes;

use Drupal\frmwrk_decoupled\FrmwrkDecoupledNodesPluginBase;

/**
 * Plugin implementation for basic_page.
 *
 * @FrmwrkDecoupledNodes(
 *   id = "basic_page",
 *   nodeType = "basic_page",
 *   resolver = "BasicPage"
 * )
 *
 * @package Drupal\frmwrk_decoupled\Plugin\FrmwrkDecoupledNodes
 */
class BasicPage extends FrmwrkDecoupledNodesPluginBase {

  /**
   * {@inheritdoc}
   */
  public function addNodes() {
    return [];
  }

}
