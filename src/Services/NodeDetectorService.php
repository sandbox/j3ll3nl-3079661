<?php

namespace Drupal\frmwrk_decoupled\Services;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\frmwrk_decoupled\FrmwrkDecoupledNodesPluginManager;

/**
 * Class NodeDetectorService.
 *
 * @package Drupal\frmwrk_decoupled\services
 */
class NodeDetectorService implements NodeDetectorServiceInterface {

  const NODES_CID = "frmwrk_decoupled.node_types";
  const NODES_DEF_CID = "frmwrk_decoupled.node_definitions";

  /**
   * Nodes plugin manager.
   *
   * @var \Drupal\frmwrk_decoupled\FrmwrkDecoupledNodesPluginManager
   */
  private $nodesPluginManager;

  /**
   * Cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private $cacheBackend;

  /**
   * NodeDetectorService constructor.
   *
   * @param \Drupal\frmwrk_decoupled\FrmwrkDecoupledNodesPluginManager $nodesPluginManager
   *   Nodes plugin manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   Cache backend.
   */
  public function __construct(FrmwrkDecoupledNodesPluginManager $nodesPluginManager, CacheBackendInterface $cacheBackend) {
    $this->nodesPluginManager = $nodesPluginManager;
    $this->cacheBackend = $cacheBackend;
  }

  /**
   * {@inheritdoc}
   */
  public function getNodeTypes(): array {
    if ($nodeTypes = $this->cacheBackend->get(self::NODES_CID)) {
      return $nodeTypes->data;
    }
    $nodeTypes = [];
    foreach ($this->nodesPluginManager->getDefinitions() as $pluginDefinition) {
      $nodeTypes[$pluginDefinition["nodeType"]] = $pluginDefinition["resolver"];
    }

    $this->cacheBackend->set(self::NODES_CID, $nodeTypes);

    return $nodeTypes;
  }

  /**
   * {@inheritdoc}
   */
  public function getNodeDefinitions(): array {
    if ($nodeDefinitions = $this->cacheBackend->get(self::NODES_DEF_CID)) {
      return $nodeDefinitions->data;
    }
    $nodeDefinitions = [];
    foreach ($this->nodesPluginManager->getDefinitions() as $pluginDefinition) {
      /** @var \Drupal\frmwrk_decoupled\FrmwrkDecoupledNodesInterface $plugin */
      $plugin = $this->nodesPluginManager->createInstance($pluginDefinition['id']);
      $nodeDefinitions[] = $plugin->getGraphqlBaseFile();
    }

    $this->cacheBackend->set(self::NODES_DEF_CID, $nodeDefinitions);

    return $nodeDefinitions;
  }

}
