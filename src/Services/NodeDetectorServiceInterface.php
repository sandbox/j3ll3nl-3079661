<?php

namespace Drupal\frmwrk_decoupled\Services;

/**
 * Interface NodeDetectorServiceInterface.
 *
 * @package Drupal\frmwrk_decoupled\services
 */
interface NodeDetectorServiceInterface {

  /**
   * Return all discovered node types.
   *
   * Results will be cached permanently, upon adding more nodes it is expected
   * to do a cache rebuild.
   *
   * @return array
   *   Node types.
   */
  public function getNodeTypes(): array;

  /**
   * Return all discovered node definitions.
   *
   * Results will be cached permanently, upon adding more nodes it is expected
   * to do a cache rebuild.
   *
   * @return array
   *   Collection of graphQL file locations.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getNodeDefinitions(): array;

}
