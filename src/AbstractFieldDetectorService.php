<?php

namespace Drupal\frmwrk_decoupled;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\frmwrk_decoupled\Services\NodeDetectorServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AbstractFieldDetectorService.
 *
 * Base class for field detection services, which purpose is to detect the field
 * names that can contain a reference to something (paragraph, taxonomy term,
 * etc.)
 *
 * @package Drupal\frmwrk_decoupled
 */
abstract class AbstractFieldDetectorService implements DetectableFieldInterface, ContainerInjectionInterface {

  /**
   * Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Node detector service.
   *
   * @var \Drupal\frmwrk_decoupled\services\NodeDetectorServiceInterface
   */
  protected $nodeDetectorService;

  /**
   * ParagraphFieldDetectorService constructor.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   Entity manager.
   * @param \Drupal\frmwrk_decoupled\services\NodeDetectorServiceInterface $nodeDetectorService
   *   Node detector service.
   */
  public function __construct(EntityFieldManagerInterface $entityFieldManager, NodeDetectorServiceInterface $nodeDetectorService) {
    $this->entityFieldManager = $entityFieldManager;
    $this->nodeDetectorService = $nodeDetectorService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_field.manager'),
      $container->get('frmwrk_decoupled.services.node_detector_service')
    );
  }

  /**
   * Transforms a PascalCase string to snake_case.
   *
   * Useful to transform GraphQL camel/pascal-case fields to drupal machine
   * names.
   *
   * @param string $string
   *   Unformatted camel/Pascal-Case string input.
   *
   * @return string
   *   Formatted string in snake_case.
   */
  public static function pascalToSnake(string $string): string {
    return strtolower(preg_replace(['/([a-z\d])([A-Z])/', '/([^_])([A-Z][a-z])/'], '$1_$2', $string));
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldNames(): array {
    $fieldNames = [];
    foreach ($this->nodeDetectorService->getNodeTypes() as $bundle) {
      if ($fields = $this->getFieldNamesByBundle($bundle)) {
        $fieldNames = array_merge($fieldNames, $fields);
      }
    }

    return isset($fieldNames) ? array_unique($fieldNames) : [$this->getDefaultFieldName()];
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldNamesByBundle(string $bundle): array {
    $fields = $this->entityFieldManager->getFieldDefinitions('node', self::pascalToSnake($bundle));
    foreach ($fields as $fieldDefinition) {
      if (
        $fieldDefinition->getSetting('target_type')
        && $fieldDefinition->getSetting('target_type') == $this->getTargetType()
      ) {
        $out[] = $fieldDefinition->get('field_name');
      }
    }

    return $out ?? [];
  }

}
