# FRMWRK Decoupled

## GraphQL example

- A query example on /Graphql endpoint

```graphql
fragment NodeBasicFields on NodeInterface {
  author
  id
  title
  url
  __typename
}

fragment ParagraphBasicFields on ParagraphTypeInterface {
  id
  parent_field_name
  __typename
}

fragment Todo on NotYetImplementedParagraph {
  bundle
}

fragment ContentParagraph on ContentParagraph {
  ... ParagraphBasicFields
  field_image {
    ... Image
  }
  field_content
  field_link {
    ... Link
  }
  field_style_options {
    ... ParagraphBasicFields
    ... FieldStyleOptions
  }
  field_title
}

fragment FieldStyleOptions on StyleOptionsParagraph {
  ... ParagraphBasicFields
  field_background_color
  field_paragraph_layout
}

fragment TabsParagraph on TabsParagraph {
  field_tab {
    parent_field_name
    field_tab_title
    field_tab_content {
      ... ParagraphBasicFields
      ... on NotYetImplementedParagraph {
        ... Todo
      }
      ... on ContentParagraph {
        ... ContentParagraph
      }
      ... on TabsParagraph {
        ... TabsParagraph
      }
      ... on ContentListParagraph {
        ... ContentListParagraph
      }
      ... on DocumentationParagraph {
        ... DocumentationParagraph
      }
      ... on VideoParagraph {
        ... VideoParagraph
      }
    }
  }
}

fragment ContentListParagraph on ContentListParagraph {
  ... ParagraphBasicFields
  field_title
  nodes {
    items {
      ... NodeBasicFields
    }
    total
  }
}

fragment DocumentationParagraph on DocumentationParagraph {
  ... ParagraphBasicFields
  field_title
  field_content
  field_file {
    ... Document 	
  }
  field_style_options {
    ... FieldStyleOptions
  }
}

fragment VideoParagraph on VideoParagraph {
  field_video {
    ... Video
  }
  field_link {
    ... Link
  }
}

fragment MediaBasicFields on MediaInterface {
  id
  filename
}

fragment Document on File {
  ... MediaBasicFields
  uri
  filemime
  filesize
}

fragment Image on Image {
  ... MediaBasicFields
  alt
  variant(style: "medium")
}

fragment Video on Video {
  ... MediaBasicFields
  id
  filename
  uri
}

fragment Metatag on metatag {
  name
  content
}

fragment Link on Link {
  title
  uri
}


query {
  config {
    systemsite {
      name
      slogan
    }
  }
  route(path: "/my-custom-alias") {
  	... NodeBasicFields
  	... on News {
  	  field_summary
  	  field_teaser_image {
  	    id
  	    filename
  	    alt
  	    variant(style: "medium")
  	  }
  	}
    ... on BasicPage {
      metatags {
        ... Metatag
      }
      paragraphs {
        ... ParagraphBasicFields
        ... on NotYetImplementedParagraph {
          ... Todo
        }
        ... on ContentParagraph {
          ... ContentParagraph
        }
        ... on TabsParagraph {
          ... TabsParagraph
        }
        ... on ContentListParagraph {
          ... ContentListParagraph
        }
        ... on DocumentationParagraph {
          ... DocumentationParagraph
        }
        ... on VideoParagraph {
          ... VideoParagraph
        }
      }
    }
  }
}
```

- A mutation example on /Graphql endpoint
```graphql
mutation {
  # Requires frmwrk_decoupled_forms
  submitTestForm(data: {webform_id: "test", entity_type: false, entity_id: false, in_draft: false, uid: 1, langcode: "en", token: "123", uri: "/", remote_addr: "127.0.0.1", data: {checkbox: true}}) {
    ... on TestForm {
      checkbox
    }
  }
}
```

## Customizing the schema
In some case extra fields could be added to paragraphs that might not be destined to end in 
the main project, here we can choose to extend a existing plugin (naming of properties and place 
files is crucial to get it to work)

A better written out example and used test case during the build is located in the docs folder 
of this repo.
