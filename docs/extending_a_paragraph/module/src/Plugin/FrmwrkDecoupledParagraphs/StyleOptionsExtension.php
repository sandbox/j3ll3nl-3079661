<?php

namespace Drupal\docs_module\Plugin\FrmwrkDecoupledParagraphs;

use Drupal\frmwrk_decoupled_paragraphs\FrmwrkDecoupledParagraphsPluginBase;

/**
 * Example for extending paragraph implementations.
 *
 * Prop breakdown:
 *   id => this must match the graphql file naming containing extension or base,
 *    eg. this plugin definition has the `style_options_paragraph_extension` id
 *    so files containing it's definitions must be named
 *    `style_options_paragraph_extension.base.graphqls` or
 *    `style_options_paragraph_extension.extension.graphqls`.
 *  description => a small description of the plugin or it's goal.
 *
 * @FrmwrkDecoupledParagraphs(
 *   id = "style_options_paragraph_extension",
 *   description = @Translation("Adds extra style options type paragraphs.")
 * )
 *
 * @package Drupal\docs_module\Plugin\FrmwrkDecoupledParagraphs;
 */
class StyleOptionsExtension extends FrmwrkDecoupledParagraphsPluginBase {

  /**
   * {@inheritdoc}
   */
  public function addParagraphs(): array {
    return [
      // This matches the graphql type you're looking to extend.
      'StyleOptionsParagraph' => [
        // The field(s) you added => the field resolver, usually the default
        // resolver gets the job done.
        'field_test' => 'paragraph_field_resolver',
      ],
    ];
  }

}
